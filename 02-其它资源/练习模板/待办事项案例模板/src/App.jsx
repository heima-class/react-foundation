import Head from './components/head'
import Main from './components/main'
import Foot from './components/foot'
function App() {
  return (
    <section className="todoapp">
      {/* 头部 */}
      <Head />
      {/* 主体部分 */}
      <Main />
      {/* 底部 */}
      <Foot />
    </section>
  )
}

export default App
