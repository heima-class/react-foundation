function Head() {
  return (
    <header className="header">
      <h1>待办事项</h1>
      <input className="new-todo" placeholder="输入任务名" autoFocus />
    </header>
  )
}

export default Head
