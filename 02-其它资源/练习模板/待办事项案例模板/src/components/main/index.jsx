function Main() {
  // 假数据
  const list = [
    { id: 1, text: '吃饭', done: true },
    { id: 2, text: '学习', done: false },
    { id: 3, text: '睡觉', done: true },
  ]
  return (
    <section className="main">
      <input id="toggle-all" className="toggle-all" type="checkbox" />
      <label htmlFor="toggle-all" />
      {/* 待办事项列表 */}
      <ul className="todo-list">
        {list.map((item) => (
          <li className={item.done ? 'completed' : ''} key={item.id}>
            <div className="view">
              {/* 选择框：已完成 | 未完成 */}
              <input className="toggle" type="checkbox" />
              {/* 任务名 */}
              <label>{item.text}</label>
              {/* 删除按钮 */}
              <button className="destroy"></button>
            </div>
          </li>
        ))}
      </ul>
    </section>
  )
}

export default Main
