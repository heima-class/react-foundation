import { Channel } from './components/Channel'
import { ArticleList } from './components/ArticleList'
function Home() {
  return (
    <div className="app">
      <Channel />
      <ArticleList />
    </div>
  )
}

export default Home
