import { createRoot } from 'react-dom/client'
// 样式文件
import './styles/index.css'
import App from './App'

createRoot(document.getElementById('root')).render(<App />)
