import { useRoutes } from 'react-router-dom'
import { routes } from './router'
const App = () => {
  const ele = useRoutes(routes)
  return <div>{ele}</div>
}

export default App
