import axios from 'axios'
// 1. 创建新axios实例
const request = axios.create({
  baseURL: 'http://geek.itheima.net/v1_0'
})

// 2. 导出
export default request