import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import avatar from '../../../assets/back.jpg'
import { getNewsAction } from '../../../store/modules/news'

export const ArticleList = () => {
  const dis = useDispatch()
  const { active } = useSelector(state => state.channel)
  useEffect(() => {
    console.log('频道切换了：', active)
    dis(getNewsAction(active))
  }, [active, dis])
  const { list } = useSelector(state => state.news)
  return (
    <div className="list">
      {list.map(item => (
        <div key={item.art_id} className="article_item">
          <h3>{item.title}</h3>
          <div className="img_box">
            <img
              src={item.cover.type === 0 ? avatar : item.cover.images[0]}
              className="w100"
              alt=""
            />
          </div>
          <div className="info_box">
            <span>{item.aut_name}</span>
            <span>{item.comm_count}评论</span>
            <span>{item.pubdate}</span>
          </div>
        </div>
      ))}
    </div>
  )
}
