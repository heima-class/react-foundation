import { useEffect } from "react"
import { useDispatch, useSelector } from "react-redux"
import { getChannelAction, toggleChannel } from "../../../store/modules/channel"

export const Channel = () => {
  const dispatch = useDispatch()
  // 获取后台数据
  useEffect(() => {
    dispatch(getChannelAction())
  }, [dispatch])

  // 获取redux频道数据
  const { list, active } = useSelector(state => state.channel)

  return (
    <ul className="category">
      {
        list.map(item => (<li onClick={() => {
          // 需求：当前被点击的频道高亮显示=》添加select类名
          // 核心：定义一个变量flag，存储当前点击频道的ID
          dispatch(toggleChannel(item.id))

        }} key={item.id} className={active === item.id ? 'select' : ''}>{item.name}</li>))
      }
    </ul>
  )
}
