import { Channel } from './components/Channel'
import { ArticleList } from './components/ArticleList'
function Home() {
  return (
    <div className="app">
      {/* 1. 频道菜单 */}
      <Channel />
      {/* 2. 新闻列表 */}
      <ArticleList />
    </div>
  )
}

export default Home
