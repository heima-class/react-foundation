import { createSlice } from '@reduxjs/toolkit'
import request from '../../utils/request'

export const channel = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'channel',
  // 1. 定义变量(状态数据)
  initialState: {
    // 频道数据
    list: [],
    // 当前点击的频道
    active: 0
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    setChannel (state, action) {
      state.list = action.payload
    },
    toggleChannel (state, { payload }) {
      state.active = payload
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { setChannel, toggleChannel } = channel.actions

// 3. 异步action
export const getChannelAction = (payload) => {
  return async (dispatch, getState) => {
    const { data } = await request.get('/channels')
    console.log('频道数据：', data.data.channels)
    dispatch(setChannel(data.data.channels))
  }
}

// 导出reducer(创建store使用)
export default channel.reducer