import { createSlice } from '@reduxjs/toolkit'
import request from '../../utils/request'

export const news = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'news',
  // 1. 定义变量(状态数据)
  initialState: {
    list: []
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    setList (state, action) {
      state.list = action.payload
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { setList } = news.actions

// 3. 异步action
export const getNewsAction = (id) => {
  return async (dispatch, getState) => {
    const { data } = await request.get(
      `/articles?channel_id=${id}&timestamp=${Date.now()}`
    )
    dispatch(setList(data.data.results))
  }
}

// 导出reducer(创建store使用)
export default news.reducer