import { configureStore } from '@reduxjs/toolkit'
import channel from './modules/channel'
import news from './modules/news'
export default configureStore({
  reducer: {
    channel,
    news
  }
})