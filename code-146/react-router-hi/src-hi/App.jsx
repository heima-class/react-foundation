import { BrowserRouter, useRoutes, Link } from 'react-router-dom'
import './App.css'
import { routes } from './router'
/**
 * react路由配置方式：
 * 1. 组件式
 * 步骤：
 *  1. 安装：`npm i react-router-dom`
    2. 导入路由的三个核心组件：BrowserRouter(HashRouter) / Routes / Route
    3. 使用 BrowserRouter(HashRouter) 组件包裹整个应用
    4. 使用 Link 组件作为导航菜单（路由入口）
    5. 使用 Route 组件配置路由规则和要展示的组件（路由出口）

 * 2. 集中式
   1. 准备一个配置路由规则的数组： [{path:'地址', element:组件}, ...]
   2. 使用useRoutes钩子函数生成路由规则的组件元素：const ele = useRoutes(路由规则的数组)
   3. 在根组件中渲染useRoutes钩子函数生成路由规则的组件元素=》== 生效 ==
   4. 在index.js入口使用BrowserRouter(HashRouter)包裹App根组件
 */

function App() {
  const ele = useRoutes(routes)
  return (
    // 1. hash模式
    // <HashRouter />
    // 2. history模式
    <>
      {/* 菜单 */}
      {/* <nav className="menu">
        <ul>
          <li>
            <Link to="/">首页</Link>
          </li>
          <li>
            <Link to="/about">关于我们</Link>
          </li>
          <li>
            <Link to="/contact">联系我们</Link>
          </li>
        </ul>
      </nav> */}
      {/* 配置路由规则 */}
      <div className="box">
        {/* <Routes>
          <Route path="/" element={<Home />} />
          <Route path="/about" element={<About />} />
          <Route path="/contact" element={<Contact />} />
        </Routes> */}
        {ele}
      </div>
    </>
  )
}

export default App
