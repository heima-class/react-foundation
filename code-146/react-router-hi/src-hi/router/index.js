// 导入路由组件
import Home from '../views/home'
import About from '../views/about'
import Contact from '../views/contact'
import Layout from '../views/layout'
import Login from '../views/login'
import Detail from '../views/detail'
import NotFound from '../views/404'

// 路由规则数组
const routes = [
  {
    path: '/',
    // 父路由
    element: <Layout />,
    children: [
      // 子路由的组件是在父路由组件中挂载渲染
      // 默认加载渲染一个子路由：path = 空 | 和父路由地址一样
      {
        path: '/',
        element: <Home />,
      },
      {
        path: '/about',
        element: <About />,
      },
      // 动态路由=> /path/:参数名1/:参数名2
      {
        path: '/detail/:id/:name',
        element: <Detail />,
      },
      {
        path: '/contact',
        element: <Contact />,
      },
    ]
  },
  {
    path: '/login',
    element: <Login />
  },
  // 配置404页面
  {
    path: '*',  // * 通配符=》不存在的path地址，会被*匹配
    element: <NotFound />
  },
  // {
  //   path: '/home',
  //   element: <Home />,
  // },
  // {
  //   path: '/about',
  //   element: <About />,
  // },
  // {
  //   path: '/contact',
  //   element: <Contact />,
  // },
]

export { routes }