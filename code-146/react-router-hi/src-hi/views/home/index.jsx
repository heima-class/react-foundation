import { useLocation, useSearchParams } from 'react-router-dom'

function Home() {
  // 1. 获取state跳转传参(推荐)
  const location = useLocation()
  console.log('location:', location)
  // 2. 获取query参数
  const [query] = useSearchParams()
  console.log('query:', query.get('b'))
  return (
    <div>
      <h1>首页</h1>
    </div>
  )
}

export default Home
