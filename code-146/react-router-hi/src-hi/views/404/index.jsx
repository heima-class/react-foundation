import { Link } from 'react-router-dom'

function NotFound() {
  return (
    <div>
      <h1>
        迷路了吗？<Link to="/">带你回家</Link>
      </h1>
    </div>
  )
}

export default NotFound
