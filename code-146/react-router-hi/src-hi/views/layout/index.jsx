import { Outlet, Link } from 'react-router-dom'
import './index.css'
function Layout() {
  return (
    <div>
      <h1>公共布局页面</h1>
      {/* 菜单 */}
      <nav className="menu">
        <ul>
          <li>
            <Link to="/">首页</Link>
          </li>
          <li>
            <Link to="/about">关于我们</Link>
          </li>
          <li>
            <Link to="/contact">联系我们</Link>
          </li>
        </ul>
      </nav>
      {/* 显示子路由页面 */}
      <div className="child">
        {/* Outlet === vue中 router-view */}
        <Outlet />
      </div>
    </div>
  )
}

export default Layout
