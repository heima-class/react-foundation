import { useNavigate } from 'react-router-dom'
function Login() {
  const nav = useNavigate()

  const login = () => {
    /**
     * 1. 发送请求=》获取token
     * 2. 跳转首页
     * 语法：
     * 通过 `useNavigate` hook 来拿到路由提供跳转方法
     * const nav = useNavigate()
     * nav('path地址', {replace?: boolean, state?: any})
     */
    // 1. 跳转方式
    // nav('/') // push跳转
    // nav({ pathname: '/' }) // push跳转
    nav('/', { replace: true }) // replace跳转
    // 2. 跳转传参
    nav('/?a=1&b=2', { state: 123 })
  }
  return (
    <div>
      <h1>login</h1>
      <button onClick={login}>登录</button>
    </div>
  )
}

export default Login
