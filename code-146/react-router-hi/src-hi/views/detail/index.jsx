import { useParams } from 'react-router-dom'

function Detail() {
  const params = useParams()
  console.log('动态路由参数：', params)
  return (
    <div>
      <h1>详情</h1>
    </div>
  )
}

export default Detail
