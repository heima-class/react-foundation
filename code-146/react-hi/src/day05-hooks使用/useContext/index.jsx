import { createContext, useState, useContext } from 'react'
/**
 * 跨多级组件通信：
 * 1. Provider: 提供共享数据 =》<Provider value={data}></Provider>
 * 2. Consumer: 接收共享数据 =》<Consumer>{callback}</Consumer>
 *
 * react hooks提供了一个钩子函数：const data = useContext(传入创建的context对象)(推荐)
 */
// 1. 创建context对象
const DataContext = createContext()

function GrandSon(params) {
  // 现在：使用useContex在js中获取共享数据
  const { count, setCount } = useContext(DataContext)
  // console.log('js中获取共享数据：', data1)
  return (
    <div>
      <h3
        onClick={() => {
          setCount(count + 1)
        }}
      >
        孙子:{count}
      </h3>
      {/* 之前：在jsx中以组件形式获取data共享数据 */}
      <DataContext.Consumer>
        {(data) => {
          return <h2>共享数据：{data.count}</h2>
        }}
      </DataContext.Consumer>
    </div>
  )
}

function Son(params) {
  return (
    <div>
      <h2>儿子</h2>
      <hr />
      <GrandSon />
    </div>
  )
}

function App() {
  const [count, setCount] = useState(0)
  return (
    <DataContext.Provider
      value={{
        count,
        setCount,
      }}
    >
      <h1>useContext使用-爷爷</h1>
      <hr />
      <Son />
    </DataContext.Provider>
  )
}

export default App
