import axios from 'axios'
import { useEffect, useState } from 'react'/**
 * 自定义hooks
 * 作用：状态逻辑复用
 * 语法：useXXX() 
 * 说明❓：自定义hooks钩子函数，必须以use开头，函数内部使用官方提供的钩子函数（useState、useEffect等）
 */

const useList = () => {
  const [list, setList] = useState([])
  useEffect(() => {
    // 写法1
    // 1. 定义发请求方法
    // const getNews = async () => {
    //   const { data } = await axios.get('http://ajax-api.itheima.net/api/news')
    //   console.log('后台新闻：', data)
    //   setList(data.data)
    // }
    // // 2. 调用
    // getNews()

    // 写法2：借助函数自调用IIFE(立即执行函数表达式)
    ; (async () => {
      const { data } = await axios.get('http://ajax-api.itheima.net/api/news')
      console.log('后台新闻：', data)
      setList(data.data)
    })()
  }, [])

  return { list }
}

export { useList }