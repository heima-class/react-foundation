import { useState } from 'react'

const Count = () => {
  /**
   * hooks函数组件的执行：
   * 1. 第一次会从头到尾执行一遍，定义的变量的默认值会生效
   * 2. 修改了变量，再次从头到尾执行一遍
   *    例如：useState(0) 再次执行，默认值会覆盖之前修改的值？不会
   * 说明❓：useState()定义变量传入的默认值，只会在组件第一次加载渲染时生效
   */
  const [count, setCount] = useState(0)
  console.log('Count组件重新执行了')

  // 错误演示
  // const fn = () => {
  //   const [count, setCount] = useState(0)
  //   console.log(count)
  // }

  return (
    <div>
      <h1>计数器：{count}</h1>
      <button onClick={() => setCount(count + 1)}>+1</button>
    </div>
  )
}

export default Count
