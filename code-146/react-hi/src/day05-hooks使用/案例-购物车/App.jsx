import 'bootstrap/dist/css/bootstrap.css'
import './App.scss'
// 导入子组件
import Head from './components/head'
import Item from './components/item'
import Foot from './components/foot'

import request from './utils/request'
import { useEffect, useState } from 'react'
import { CountContext } from './utils'

export default function App() {
  /**
   * 函数组件更新执行过程：
   * 1. 组件挂载（1次）：从头到尾执行一遍
   * 2. 组件修改了状态（多次）：从头到尾执行一遍
   */
  console.log('App函数组件执行了')
  // 1. 购物车列表数据
  const [list, setList] = useState([])
  useEffect(() => {
    ;(async () => {
      const {
        data: { list },
      } = await request.get('/api/cart')
      console.log('购物车列表：', list)
      setList(list)
    })()
  }, [])

  // 2. 修改商品选中的方法
  /**
   *
   * @param {*} goods_id 商品ID
   * @param {*} goods_state 商品选中状态
   */
  const changeSel = (goods_id, goods_state) => {
    setList(
      list.map((item) => {
        if (item.goods_id === goods_id) {
          return {
            ...item,
            goods_state,
          }
        }
        return item
      })
    )
  }

  // 3. 修改商品数量
  const changeCount = async (id, goods_count) => {
    setList(
      list.map((item) => {
        if (item.goods_id === id) {
          return {
            ...item,
            goods_count,
          }
        }
        return item
      })
    )
  }

  const totalCount = list.reduce((count, item) => {
    // 只累加选中的商品
    if (item.goods_state) {
      return count + item.goods_count
    }
    return count
  }, 0)

  const totalPrice = list.reduce((count, item) => {
    if (item.goods_state) {
      return count + item.goods_count * item.goods_price
    }
    return count
  }, 0)

  return (
    <CountContext.Provider value={{ changeCount }}>
      <div className="app">
        {/* 1. 标题 */}
        <Head />
        {/* 2. 商品列表项 */}
        {list.map((item) => (
          // <Item key={item.goods_id} good={item} />
          // {...item} 相当于把对象中所有属性和值都传递给子组件
          <Item changeSel={changeSel} key={item.goods_id} {...item} />
        ))}
        {/* 3. 底部 */}
        <Foot totalCount={totalCount} totalPrice={totalPrice} />
      </div>
    </CountContext.Provider>
  )
}
