import axios from 'axios'
// 1. 创建新axios实例
const request = axios.create({
  baseURL: 'https://applet-base-api-t.itheima.net/'
})

// 2. 导出
export default request