import './index.scss'
import { CountContext } from '../../utils'
import { useContext } from 'react'
function Count({ id, count }) {
  // 3. 获取修改数量方法
  const { changeCount } = useContext(CountContext)
  return (
    <div className="my-counter">
      <button
        disabled={count === 1}
        onClick={() => changeCount(id, count - 1)}
        type="button"
        className="btn btn-light"
      >
        -
      </button>
      <input
        value={count}
        onChange={(e) => changeCount(id, +e.target.value)}
        type="input"
        className="form-control inp"
      />
      <button
        onClick={() => changeCount(id, count + 1)}
        type="button"
        className="btn btn-light"
      >
        +
      </button>
    </div>
  )
}

export default Count
