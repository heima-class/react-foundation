import './index.scss'
function Foot({ totalCount, totalPrice }) {
  return (
    <div className="my-footer">
      <div>
        <span>合计:</span>
        <span className="price">¥ {totalPrice}</span>
      </div>
      <button type="button" className="footer-btn btn btn-primary">
        结算 ({totalCount})
      </button>
    </div>
  )
}

export default Foot
