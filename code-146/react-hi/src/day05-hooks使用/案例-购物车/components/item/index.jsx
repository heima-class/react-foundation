import './index.scss'
import Count from '../count'
function Item({
  goods_count,
  goods_img,
  goods_name,
  goods_price,
  goods_state,
  goods_id,
  changeSel,
}) {
  return (
    // 展示单个商品
    <div className="my-goods-item">
      <div className="left">
        <div className="custom-control custom-checkbox">
          {/* 商品选择 */}
          <input
            type="checkbox"
            className="custom-control-input"
            checked={goods_state}
            onChange={(event) => {
              console.log(event.target.checked)
              // 修改当前商品的选中状态=》调用父组件方法
              changeSel(goods_id, event.target.checked)
            }}
            id={`input-${goods_id}`}
          />
          <label className="custom-control-label" htmlFor={`input-${goods_id}`}>
            <img src={goods_img} alt="" />
          </label>
        </div>
      </div>
      <div className="right">
        <div className="top">{goods_name}</div>
        <div className="bottom">
          <span className="price">¥ {goods_price}</span>
          <span>
            <Count id={goods_id} count={goods_count} />
          </span>
        </div>
      </div>
    </div>
  )
}

export default Item
