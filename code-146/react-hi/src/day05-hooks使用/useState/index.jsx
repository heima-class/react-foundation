import { useState } from 'react'
function App() {
  /**
   * useState钩子函数:
   * 说明❓：定义任意类型的变量
   * == 语法：const [变量, 修改变量的方法] = useState(变量的默认值) ==
   * react修改数据思想：数据不可变=》新值覆盖老值
   */
  // 1. 简单类型
  const [count, setCount] = useState(0)
  const [isShow, setShow] = useState(true)

  // 2. 复杂类型
  const [person, setPerson] = useState({
    name: '小红',
    age: 18,
  })
  const [list, setList] = useState([1, 2, 3])

  // 修改变量
  const add = () => {
    setCount(count + 1)
    setShow(!isShow)
    setPerson({
      ...person,
      age: 30,
    })
    setList([Math.random(), ...list])
  }
  return (
    <div>
      <h1>useState钩子函数</h1>
      <hr />
      <ul>
        <li>{count}</li>
        <li>
          <button onClick={add}>add</button>
        </li>
        <li>{isShow ? 'true' : 'false'}</li>
        <li>
          {person.name} - {person.age}
        </li>
        <li>{list.join('-')}</li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
    </div>
  )
}

export default App
