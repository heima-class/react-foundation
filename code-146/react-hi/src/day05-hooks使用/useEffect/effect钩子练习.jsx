import { useList } from '../hooks'

function App() {
  /**
   * 需求：获取http://ajax-api.itheima.net/api/news数据并渲染
   * 思考❓：请求查询数据，什么时候发送？=》组件挂载查询一次
   */
  const { list } = useList()

  return (
    <div>
      <ul>
        {list.map((item) => (
          <li key={item.id}>
            <p>
              <img src={item.img} alt={item.title} />
            </p>
            <p>{item.title}</p>
          </li>
        ))}
      </ul>
    </div>
  )
}

export default App
