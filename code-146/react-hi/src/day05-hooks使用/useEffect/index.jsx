import { useEffect, useState } from 'react'

function App() {
  /**
   * useEffect处理副作用
   * 语法1：useEffect(回调函数)
   * 说明❓：
   * 1. 传入的回调函数默认会执行一次 =》componentDidMount()
   * 2. 修改了变量，会再次执行回调函数 => componentDidUpdate()
   * 思考❓：像类组件中那两个生命周期钩子函数？componentDidMount()+componentDidUpdate()
   *
   * 语法2：useEffect(回调函数, [依赖的变量1,依赖的变量2,...])
   * 说明❓：
   * 1. 传入的回调函数默认会执行一次 =》componentDidMount()
   * 2. 数组内依赖的变量发生了变化，才会再次执行
   *
   * 语法3：useEffect(回调函数, []) => 重点掌握
   * 说明❓：
   * 1. 相当于componentDidMount(),组件挂载执行一次
   * 2. 回调函数内部返回的另外一个callback相当于componentWillUnMount()
   */
  const [count, setCount] = useState(1)

  const [obj, setObj] = useState({ age: 18 })
  useEffect(() => {
    // 处理副作用
    console.log('处理副作用')
    document.title = `标题修改次数：${count}`
  }, [count])

  useEffect(() => {
    // 作用：1. 发送请求
    console.log('相当于componentDidMount(),组件挂载执行一次')
    return () => {
      // 作用：2. 清除定时器或解绑事件等
      console.log('相当于componentWillUnMount(),组件销毁执行')
    }
  }, [])

  // 点击加一
  const add = () => {
    setCount(count + 1)
  }
  // 修改对象
  const changeObj = () => {
    setObj({
      ...obj,
      age: Math.random(),
    })
  }
  return (
    <div>
      <h1>useEffect处理副作用</h1>
      <p>{count}</p>
      <p>{obj.age}</p>
      <p>
        <button onClick={add}>add</button>
      </p>
      <p>
        <button onClick={changeObj}>changeObj</button>
      </p>
    </div>
  )
}

export default App
