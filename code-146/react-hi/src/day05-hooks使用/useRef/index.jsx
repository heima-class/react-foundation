import { useEffect } from 'react'
import { useRef } from 'react'

function App() {
  // 1. 创建ref对象
  const h1 = useRef(null)

  useEffect(() => {
    // 3. 组件挂载：获取dom对象
    console.log('h1的dom：', h1)
    h1.current.style.fontSize = '100px'
  }, [])
  return (
    <div>
      {/* 2. 绑定ref对象 */}
      <h1 ref={h1}>useRef使用</h1>
    </div>
  )
}

export default App
