import { Component } from 'react'
import Title from './title'
function Son1(props) {
  console.log('函数组件接收插槽内容', props)
  return (
    <div>
      <h2>子组件1</h2>
      <ul>
        <li>{props.children}</li>
      </ul>
    </div>
  )
}

class Consumer extends Component {
  render() {
    console.log('类组件接收插槽内容：', this.props)
    return (
      <div>
        <h2
          onClick={() => {
            this.props.children(1e6)
          }}
        >
          子组件2
        </h2>
      </div>
    )
  }
}

class App extends Component {
  /**
   * children 属性使用：
   * 类似vue的插槽：<Son> 插槽html内容 </Son> =》 Son组件内部通过slot接收
   * 1. children 属性：表示该组件的子节点，只要组件有子节点，props就有该属性
     2. children 属性与普通的 props 一样，值可以是任意值（文本、React元素、组件，甚至是函数）
   * @returns 
   */
  render() {
    return (
      <div>
        {/* 需求：封装一个title组件，组件的title支持定制 */}
        <Title title="react插槽">react插槽</Title>
        <h1>children属性</h1>
        <hr />
        <Son1>
          123
          <h1>hi 插槽</h1>
        </Son1>
        <hr />
        <Consumer>
          {(data) => {
            console.log(data)
          }}
        </Consumer>
      </div>
    )
  }
}
export default App
