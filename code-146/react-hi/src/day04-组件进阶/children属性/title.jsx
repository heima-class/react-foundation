function Title({ title = '默认值', children }) {
  return (
    <div>
      <h1>{children}</h1>
    </div>
  )
}

export default Title
