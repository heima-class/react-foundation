import { Component } from 'react'

class App extends Component {
  state = {
    count: 1,
  }
  /**
   * == setState({})写法 ==
   * 理解 setState 延迟更新数据:
   * 1. 方法调用后，下一行获取到是上一次的数据
   * 2. 如果多次调用 setState 更新状态，**状态会进行合并，后面覆盖前面**
   */
  add1 = () => {
    this.setState({
      count: this.state.count + 1,
    })
    this.setState(
      {
        count: this.state.count + 2,
      },
      () => {
        // 获取最新状态数据
        console.log('最新的1：', this.state)
      }
    )
    this.setState(
      {
        count: this.state.count + 3,
      },
      () => {
        // 获取最新状态数据
        console.log('最新的2：', this.state)
      }
    )
    console.log('获取count是上一次的:', this.state.count)
  }
  /**
   * == setState(回调函数)写法 ==
   * 1. 方法调用后，下一行获取到是上一次的数据
   * 2. 如果多次调用 setState 更新状态，每次都会执行
   */
  add2 = () => {
    this.setState((prevState) => {
      return {
        count: prevState.count + 1,
      }
    })
    this.setState(
      (prevState) => {
        return {
          count: prevState.count + 2,
        }
      },
      () => {
        // 获取最新状态数据
        console.log('最新的1：', this.state)
      }
    )
    this.setState(
      (prevState) => {
        return {
          count: prevState.count + 3,
        }
      },
      () => {
        // 获取最新状态数据
        console.log('最新的2：', this.state)
      }
    )
    console.log('获取count：', this.state.count)
  }
  render() {
    return (
      <div>
        <h1>setState进阶</h1>
        <ul>
          <li>{this.state.count}</li>
          <li>
            <button onClick={this.add1}>add1</button>
          </li>
          <li>
            <button onClick={this.add2}>add2</button>
          </li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    )
  }
}
export default App
