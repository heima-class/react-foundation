import { Component } from 'react'
// 1. 导入props校验包：提供校验的类型（type）
import PropTypes from 'prop-types'

import './class'

// 需求：Colors组件接收一个数组，例如：['red','blue','green']
function Colors({ list = ['pink'], a }) {
  return (
    <div>
      <p>{a}</p>
      <ul>
        {list.map((item) => (
          <li key={item} style={{ background: item, color: '#fff' }}>
            {item}
          </li>
        ))}
      </ul>
    </div>
  )
}
// 2. 使用`组件名（函数名或类名）.propTypes = {}` 来给组件 List 的props添加校验规则
/**
 * props校验常用类型：
 * 1. 常见类型：array、bool、func、number、object、string
    2. React元素类型：element(jsx)
    3. 必填项：isRequired
    4. 特定结构的对象：shape({}) => 针对对象类型，做更加精细化的校验
 */
Colors.propTypes = {
  // 父传子属性名：数据类型
  list: PropTypes.array,
  a: PropTypes.number.isRequired,
  b: PropTypes.element,
  // 1. 只校验是一个对象
  // obj: PropTypes.object,
  // 2. 即校验是个对象也可以精细化校验对象内部属性值的类型
  obj: PropTypes.shape({
    a: PropTypes.func,
    b: PropTypes.string,
  }),
}

// 3. 设置props默认值
Colors.defaultProps = {
  // list: ['orange'],
  a: 1000,
}

// 类组件
class Child extends Component {
  // 写法2
  // 定义静态属性
  static propTypes = {
    a: PropTypes.number,
  }

  static defaultProps = {
    a: 100,
  }

  render() {
    return (
      <div>
        <h2>类组件:{this.props.a}</h2>
      </div>
    )
  }
}
// 写法1=》静态属性
// Child.propTypes = {
//   a: PropTypes.number,
// }

// Child.defaultProps = {
//   a: 100,
// }

class App extends Component {
  state = {
    list: ['red', 'blue', 'green'],
    // list: 'red',
  }

  render() {
    return (
      <div>
        <h1>props校验</h1>
        <hr />
        <Colors
          // list={this.state.list}
          obj={{ a: () => 1, b: '2' }}
          b={<h1>hi jsx</h1>}
        />
        <Child />
      </div>
    )
  }
}
export default App
