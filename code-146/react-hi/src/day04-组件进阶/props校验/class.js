/**
 * 静态属性
 */
// 1. 创建一个类
class Test {

  static c = () => {
    console.log(2)
  }

  a = () => {
    console.log(1)
  }
}

// 添加静态属性b=> 不需要实例化就能访问
Test.b = 123


// 2. 实例化
const _test = new Test()



console.log('Test类实例：', _test, Test.b)
Test.c()

