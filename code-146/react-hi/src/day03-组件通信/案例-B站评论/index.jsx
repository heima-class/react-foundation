import { Component } from 'react'
import './index.css'

// 导入子组件
import Tab from './components/tab'
import Form from './components/form'
import List from './components/list'

class App extends Component {
  // data数据
  state = {
    // content: '',
    // hot: 热度排序  time: 时间排序
    tabs: [
      {
        id: 1,
        name: '热度',
        type: 'hot',
      },
      {
        id: 2,
        name: '时间',
        type: 'time',
      },
    ],
    // 当前选中页签的标识
    active: 'time',
    // 2. 从本地获取
    list: JSON.parse(localStorage.getItem('bp')) || [
      {
        id: 1,
        author: '刘德华',
        comment: '给我一杯忘情水',
        time: new Date('2021-10-10 09:09:00'),
        // 1: 点赞 0：无态度 -1:踩
        attitude: 1,
      },
      {
        id: 2,
        author: '周杰伦',
        comment: '哎哟，不错哦',
        time: new Date('2021-10-11 09:09:00'),
        // 1: 点赞 0：无态度 -1:踩
        attitude: 0,
      },
      {
        id: 3,
        author: '五月天',
        comment: '不打扰，是我的温柔',
        time: new Date('2021-10-11 10:09:00'),
        // 1: 点赞 0：无态度 -1:踩
        attitude: -1,
      },
    ],
  }

  componentDidUpdate() {
    console.log('list变化了：', this.state.list)
    // 1. 存储的变化的评论数据
    localStorage.setItem('bp', JSON.stringify(this.state.list))
  }

  /**
   *
   * @param {*} type 当前点击页签的标识：1. hot 2. time
   */
  changeTab = (type) => {
    this.setState({
      active: type,
    })
  }
  /**
   *
   * @param {*} id 当前删除评论ID
   */
  del = (id) => {
    // // 1. 准备一个新的list（排除当前传入id评论）
    // let newList = this.state.list.filter((item) => item.id !== id);
    // // 2. 调用setState方法
    // this.setState({
    //   list: newList,
    // });
    this.setState({
      list: this.state.list.filter((item) => item.id !== id),
    })
  }

  // 添加评论
  add = (content) => {
    const newComment = {
      id: Date.now(),
      author: '作者',
      comment: content,
      time: new Date(),
      // 1: 点赞 0：无态度 -1:踩
      attitude: 0,
    }
    this.setState({
      list: [newComment, ...this.state.list],
    })
  }
  // 修改评论点赞和踩
  changeAttitude = (id, attitude) => {
    this.setState({
      list: this.state.list.map((item) => {
        if (item.id === id) {
          return {
            ...item,
            attitude,
          }
        } else {
          return item
        }
      }),
    })
  }

  render() {
    return (
      <div className="App">
        <div className="comment-container">
          {/* 评论数 */}
          <div className="comment-head">
            <span>{this.state.list.length} 评论</span>
          </div>
          {/* 排序=>1. 页签组件 */}
          <Tab
            tabs={this.state.tabs}
            changeTab={this.changeTab}
            active={this.state.active}
          />
          {/* 添加评论=> 2. 添加评论 */}
          <Form add={this.add} />
          {/* 评论列表=> 3. 评论列表 */}
          <List
            list={this.state.list}
            del={this.del}
            changeAttitude={this.changeAttitude}
          />
        </div>
      </div>
    )
  }
}
export default App
