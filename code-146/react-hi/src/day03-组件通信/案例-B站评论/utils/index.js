import dayjs from 'dayjs'

const formatTime = (time) => {
  return dayjs(time).format('YYYY年MM月DD')
}

export { formatTime }