import avatar from '../images/avatar.png'
import { formatTime } from '../utils'
/**
 *
 * @param {*} list = 设置默认值
 * @returns
 */
function List({ list = [], del, changeAttitude }) {
  /**
   * 组件重构（优化）？
   * 被拆分出来的组件用到父组件中哪些数据和方法？
   * 1. 评论列表list
   * 2. 点赞和踩方法
   * 3. 删除评论方法
   */
  return (
    <div className="comment-list">
      {list.map((item) => (
        <div key={item.id} className="list-item">
          <div className="user-face">
            <img className="user-head" src={avatar} alt="" />
          </div>
          <div className="comment">
            <div className="user">{item.author}</div>
            <p className="text">{item.comment}</p>
            <div className="info">
              {/* <span className="time">{item.time.toString()}</span> */}
              <span className="time">{formatTime(item.time)}</span>
              {/* 
       点赞高亮的处理：1 代表点赞 |   -1 代表踩 |  0 代表无态度(无需处理)
       */}
              <span
                onClick={() => {
                  changeAttitude(item.id, item.attitude === 1 ? 0 : 1)
                }}
                className={item.attitude === 1 ? 'like liked' : 'like'}
              >
                <i className="icon" />
              </span>
              <span
                onClick={() => {
                  changeAttitude(item.id, item.attitude === -1 ? 0 : -1)
                }}
                className={['hate', item.attitude === -1 ? 'hated' : ''].join(
                  ' '
                )}
              >
                <i className="icon" />
              </span>
              <span
                onClick={() => {
                  // this.del(item.id)
                  del(item.id)
                }}
                className="reply btn-hover"
              >
                删除
              </span>
            </div>
          </div>
        </div>
      ))}
    </div>
  )
}

export default List
