function Tab({ tabs, changeTab, active }) {
  /**
   * 1. tabs数据
   * 2. changeTab方法
   * 3. active数据
   */
  return (
    <div className="tabs-order">
      <ul className="sort-container">
        {tabs.map((item) => (
          <li
            onClick={() => changeTab(item.type)}
            className={item.type === active ? 'on' : ''}
            key={item.id}
          >
            按{item.name}排序
          </li>
        ))}
      </ul>
    </div>
  )
}

export default Tab
