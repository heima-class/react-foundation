import { Component } from 'react'
import avatar from '../images/avatar.png'

class Form extends Component {
  state = {
    content: '',
  }
  // 评论输入框受控处理
  handleChange = (e) => {
    this.setState({
      content: e.target.value,
    })
  }
  render() {
    return (
      <div className="comment-send">
        <div className="user-face">
          <img className="user-head" src={avatar} alt="" />
        </div>
        <div className="textarea-container">
          <textarea
            cols="80"
            rows="5"
            placeholder="发条友善的评论"
            className="ipt-txt"
            value={this.state.content}
            onChange={this.handleChange}
          />
          <button
            onClick={() => {
              this.props.add(this.state.content)
              this.setState({
                content: '',
              })
            }}
            className="comment-submit"
          >
            发表评论
          </button>
        </div>
        <div className="comment-emoji">
          <i className="face"></i>
          <span className="text">表情</span>
        </div>
      </div>
    )
  }
}
export default Form
