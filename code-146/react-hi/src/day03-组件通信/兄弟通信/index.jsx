import { Component } from 'react'

// 导入一个js不带名字，直接执行
import './class'

// 兄弟1
function Brother1({ work }) {
  return (
    <div>
      <h2>兄弟1</h2>
      <hr />
      <p>公司：{work.name}</p>
      <p>工资：{work.money}</p>
    </div>
  )
}
// 兄弟2
function Brother2() {
  return (
    <div>
      <h2>兄弟2</h2>
    </div>
  )
}

// 父
class Parent extends Component {
  state = {
    // 状态提升：把兄弟组件都想要的数据，定义到父组件 =》父传子
    work: {
      name: '字节公司',
      money: 15000,
    },
  }

  render() {
    return (
      <div>
        <h1>父组件</h1>
        <hr />
        <Brother1 work={this.state.work} />
        <hr />
        <Brother2 work={this.state.work} />
      </div>
    )
  }
}
export default Parent
