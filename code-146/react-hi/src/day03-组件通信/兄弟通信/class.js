/**
 * 回顾class关键字的使用
 * class语法糖 =》背后：构造函数
 */
// 1. 定义
// es6
class Component {
  setState () {

  }
}

class MyClass extends Component {
  // 2. 原型链
  render2 () {

  }
  // 1. 实例上的方法
  fn = () => {

  }
}

// es5
function MyClass2 () {
  this.fn = () => { }
}
MyClass2.prototype.render = () => { }



// 2. 使用类
const instance1 = new MyClass2()
console.log('MyClass2的实例：', instance1)

const instance2 = new MyClass()
console.log('MyClass的实例：', instance2)



