import { createContext } from 'react'
/**
 * 跨多级组件通信(依赖注入)：
 * 使用createContext():
 * 1. Provider组件=》提供共享数据: <Provider value={传入共享数据}></Provider>
 * 2. Consumer组件=》接收共享数据：<Consumer>{回调函数}</Consumer>
 * @returns
 */
// 创建context对象(一个项目可以创建多个)
const { Provider, Consumer } = createContext()

export { Provider, Consumer }