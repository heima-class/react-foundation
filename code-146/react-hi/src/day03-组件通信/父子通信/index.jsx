import { Component } from 'react'

// 函数子组件
function Son1(props) {
  console.log('函数子组件接受父的数据：', props)
  console.log(typeof props.a)
  return (
    <div>
      <h2>函数子组件</h2>
      <ul>
        <li>{props.count}</li>
        <li>{props.tpl}</li>
        <li>
          <button
            onClick={() => {
              props.parentFn(1000)
            }}
          >
            调用父组件方法
          </button>
        </li>
      </ul>
    </div>
  )
}

// 类子组件
class Son2 extends Component {
  render() {
    console.log('类子组件接收：', this.props)
    return (
      <div>
        <h2>类子组件</h2>
        <ul>
          <li>{this.props.count}</li>
          <li></li>
          <li></li>
        </ul>
      </div>
    )
  }
}

// 父组件
/**
 * props 是只读对象**，也就是说：只能读取对象中的属性，无法修改
  1. == 单向数据流 ==，也叫做：自顶而下（自上而下）的数据流
  2. 可以传递任意数据（数字  字符串  布尔类型 数组 对象 函数 jsx）


  vue子传父：
  核心：借助自定义事件，调用父组件方法，实现数据的共享和修改
  1. 在父组件：<Son @eventName="callback" />
  2. 在子组件：this.$emit('eventName', data) => 
     == 调用父组件中定义的方法callback(data) ==
 */
class App extends Component {
  state = {
    count: 0,
    obj: {
      name: '小红',
    },
    tpl: <h2>我是h2</h2>,
  }
  // 1. 父组件定义方法(箭头函数)
  // parentFn() {
  //   console.log('我是父组件的方法', this)
  // }
  parentFn = (data) => {
    // console.log('我是父组件的方法', this)
    console.log('儿子给老子钱花：', data)
    this.setState({
      count: data,
    })
  }

  render() {
    return (
      <div>
        <h1>父组件:{this.state.count}</h1>
        <hr />
        <Son1
          a="1"
          b={2}
          c={'c'}
          parentFn={this.parentFn}
          tpl={this.state.tpl}
          obj={this.state.obj}
          count={this.state.count}
        />
        <Son2 count={this.state.count} />
      </div>
    )
  }
}
export default App
