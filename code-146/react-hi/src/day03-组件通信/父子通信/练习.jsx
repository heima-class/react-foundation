import { Component } from 'react'
import Son from './components/son'

class Parent extends Component {
  state = {
    // 需求：老子给儿子遗产：100W， 儿子想财务自由：子组件每次点击基于100w随机增加
    money: 1e6,
  }
  /**
   *
   * @param {*} addMky 累加的钱数（随机数）
   */
  changeMky = (addMky) => {
    this.setState({
      money: this.state.money + addMky,
    })
  }

  render() {
    return (
      <div>
        <h1>父组件</h1>
        <hr />
        <Son mky={this.state.money} change={this.changeMky} />
      </div>
    )
  }
}
export default Parent
