// function Son(props) {
//   return (
//     <div>
//       <h2>儿子:{props.mky}</h2>
//       <button
//         onClick={() => {
//           props.change(Math.floor(Math.random() * 1e6 + 1))
//         }}
//       >
//         问老子加钱
//       </button>
//     </div>
//   )
// }
function Son({ mky, change }) {
  return (
    <div>
      <h2>儿子:{mky}</h2>
      <button
        onClick={() => {
          change(Math.floor(Math.random() * 1e6 + 1))
        }}
      >
        问老子加钱
      </button>
    </div>
  )
}

export default Son
