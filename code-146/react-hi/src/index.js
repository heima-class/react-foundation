
import { createRoot } from 'react-dom/client'

// import MyFn from './day02-组件基础/函数组件'
// import MyClassCp from './day02-组件基础/类组件'
// import App from './day02-组件基础/类组件的状态'
// import App from './day02-组件基础/事件绑定'
// import App from './day02-组件基础/受控组件'
// import App from './day02-组件基础/非受控组件'
// import App from './day02-组件基础/案例-B站评论'
// import App from './day03-组件通信/父子通信'
// import App from './day03-组件通信/父子通信/练习'
// import App from './day03-组件通信/兄弟通信'
// import App from './day03-组件通信/context使用'
// import App from './day03-组件通信/案例-B站评论'
// import App from './day04-组件进阶/children属性'
// import App from './day04-组件进阶/props校验'
// import App from './day04-组件进阶/生命周期'
// import App from './day04-组件进阶/setState进阶'
// import App from './day05-hooks使用/useState'
// import App from './day05-hooks使用/hooks组件更新过程'
// import App from './day05-hooks使用/useEffect'
// import App from './day05-hooks使用/useEffect/effect钩子练习'
// import App from './day05-hooks使用/useContext'
// import App from './day05-hooks使用/useRef'
import App from './day05-hooks使用/案例-购物车/App'









// const box = (
//   <>
//     <MyFn />
//     <hr />
//     <MyClassCp />
//   </>
// )
createRoot(document.getElementById('root')).render(<App />)