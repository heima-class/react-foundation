import { createRoot } from 'react-dom/client'
import avatar from './images/avatar.png'
import { formatTime } from './utils'

// 1. 评论样式
import './index.css'
// 2. data数据
const state = {
  // hot: 热度排序  time: 时间排序
  tabs: [
    {
      id: 1,
      name: '热度',
      type: 'hot'
    },
    {
      id: 2,
      name: '时间',
      type: 'time'
    }
  ],
  // 当前选中页签的标识
  active: 'time',
  list: [
    {
      id: 1,
      author: '刘德华',
      comment: '给我一杯忘情水',
      time: new Date('2021-10-10 09:09:00'),
      // 1: 点赞 0：无态度 -1:踩
      attitude: 1
    },
    {
      id: 2,
      author: '周杰伦',
      comment: '哎哟，不错哦',
      time: new Date('2021-10-11 09:09:00'),
      // 1: 点赞 0：无态度 -1:踩
      attitude: 0
    },
    {
      id: 3,
      author: '五月天',
      comment: '不打扰，是我的温柔',
      time: new Date('2021-10-11 10:09:00'),
      // 1: 点赞 0：无态度 -1:踩
      attitude: -1
    }
  ]
}


// 3. 评论html结构
const element = (
  <div className="App">
    <div className="comment-container">
      {/* 评论数 */}
      <div className="comment-head">
        <span>{state.list.length} 评论</span>
      </div>
      {/* 排序 */}
      <div className="tabs-order">
        <ul className="sort-container">
          {/* <li className="on">按热度排序</li>
          <li>按时间排序</li> */}
          {
            state.tabs.map(item => <li className={item.type === state.active ? 'on' : ''} key={item.id}>按{item.name}排序</li>)
          }
        </ul>
      </div>

      {/* 添加评论 */}
      <div className="comment-send">
        <div className="user-face">
          <img className="user-head" src={avatar} alt="" />
        </div>
        <div className="textarea-container">
          <textarea
            cols="80"
            rows="5"
            placeholder="发条友善的评论"
            className="ipt-txt"
          />
          <button className="comment-submit">发表评论</button>
        </div>
        <div className="comment-emoji">
          <i className="face"></i>
          <span className="text">表情</span>
        </div>
      </div>

      {/* 评论列表 */}
      <div className="comment-list">
        {state.list.map((item) => (
          <div key={item.id} className="list-item">
            <div className="user-face">
              <img className="user-head" src={avatar} alt="" />
            </div>
            <div className="comment">
              <div className="user">{item.author}</div>
              <p className="text">{item.comment}</p>
              <div className="info">
                {/* <span className="time">{item.time.toString()}</span> */}
                <span className="time">{formatTime(item.time)}</span>
                {/* 
               点赞高亮的处理：1 代表点赞 |   -1 代表踩 |  0 代表无态度(无需处理)
               */}
                <span
                  className={
                    item.attitude === 1 ? 'like liked' : 'like'
                  }
                >
                  <i className="icon" />
                </span>
                <span
                  className={[
                    'hate',
                    item.attitude === -1 ? 'hated' : '',
                  ].join(' ')}
                >
                  <i className="icon" />
                </span>
                <span className="reply btn-hover">删除</span>
              </div>
            </div>
          </div>
        ))}
      </div>
    </div>
  </div>
)

createRoot(document.getElementById('root')).render(element)

