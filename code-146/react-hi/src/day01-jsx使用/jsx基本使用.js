// react 程序执行的入口
// 1. 导入react核心依赖
import React from 'react';
import ReactDOM from 'react-dom/client';

// 2. 创建项目根实例：ReactDOM.createRoot(spa应用根组件挂载点元素dom对象)
const root = ReactDOM.createRoot(document.getElementById('root'));

/**
 * 如何创建react的html元素？
 * 使用：React.createElement('html元素标签名',{属性名1:属性值1,属性名2:属性值2...}, 子元素1,子元素2...)
 * 注意⚠️：创建元素的css类名需要使用=》className
 */
const hello = React.createElement('h1', { a: 1, b: 2, className: 'test' }, 'hi react!', 123)
// 如何让hello元素外边包裹一个div？
const div = React.createElement('div', null, hello)

/**
 * jsx基本使用
 * 注意：
 * 1. jsx元素名都是有效的html元素
 * 2. 添加类名写成className，label的for属性写成htmlFor
 * 3. 结构复杂建议加个圆括号包裹
 * 4. jsx元素只能有一个根元素 => 幽灵节点<>子元素</>（不会渲染任何元素）
 */
const ul = (<ul className="box">
  <li>1</li>
  <li>2</li>
  <li>3</li>
  <li>
    <label htmlFor="test">选中</label>
    <input type="checkbox" id="test" />
  </li>
</ul>)

const test = <>
  <p>1</p>
  <p>2</p>
</>

/**
 * jsx中使用差值表达式（类似vue的胡子语法）
 * 语法：{变量(简单类型和复杂类型)|js表达式（有值）}
 * 注意：
 * 1. 不能直接绑定渲染对象
 * 2. 不能直接绑定渲染方法=>可以调用
 * 
 */
let abc = 123
let obj = { name: '小红', age: 18 }
let arr = [1, 2, 3]
let fn = () => {
  console.log(456)
  return 789
}
let fn2 = () => {
  const a = <h1>hi fn2</h1>
  return a
}
const box = (
  <ul>
    <li>{abc * 10}</li>
    <li>{1 + 1}</li>
    <li>{abc > 10 ? '大于10' : '小于10'}</li>
    <li>{obj.name}</li>
    <li>{arr}</li>
    <li>{arr[1]}</li>
    <li>{fn()}</li>
    <li>{fn2()}</li>
    <li>{<h2>000</h2>}</li>
    <li></li>
  </ul>
)

/**
 * jsx条件渲染：
 * 满足某个条件，就显示某个html结构
 * 1. if/else语句
 * 2. 三元表达式
 * 3. && （不太建议）
 */
/**
 * 
 * @param {*} loading 是否加载完成 true | false
 * @returns 
 */
const isLoad = (loading) => {
  if (loading) {
    return <h2>加载中...</h2>
  } else {
    return <h2>加载完成</h2>
  }
}

const box2 = (
  <div>
    <div> {isLoad(true)}</div>
    <hr />
    <div>
      {
        true ? <h2>加载中...</h2> : <h2>加载完成</h2>
      }
    </div>
    <div>
      {true && <h1>加载完成</h1>}
    </div>
  </div>
)


/**
 * jsx列表渲染
 * 需求：渲染后端方法的列表数据
 */
const list = [
  { id: 1, name: '王鑫航', salary: 2e4 },
  { id: 2, name: '刘海涛', salary: 8e4 },
  { id: 3, name: '王亦凡', salary: 1e5 }
]

// 1. for循环形式
const renderList = () => {
  // 1. 存储根据数据生成的li的html结构
  const _list = []
  list.forEach(item => {
    _list.push(<li key={item.id}>姓名：{item.name}, 期望薪资：{item.salary}</li>)
  })
  return _list
}

// 2. map循环：循环数组并生成一个新数组(推荐写法)

const listBox = (
  <>
    <div>
      {
        list.map(item =>
          <p key={item.id}>姓名：{item.name}, 期望薪资：{item.salary}</p>
        )
      }
    </div>
    <hr />
    <ul>
      {renderList()}
    </ul>
    <hr />
    <ul>
      {
        [<li key={1}>1</li>, <li key={2}>2</li>]
      }
    </ul>
  </>
)



// 3. 使用root根实例绘制用户界面（视图html）
root.render(
  listBox
);

