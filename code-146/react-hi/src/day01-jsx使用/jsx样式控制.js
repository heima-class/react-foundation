// react 程序执行的入口
// 1. 导入react核心依赖
import React from 'react';
import ReactDOM from 'react-dom/client';

// 导入样式
import './styles/index.css'

// 2. 创建项目根实例：ReactDOM.createRoot(spa应用根组件挂载点元素dom对象)
const root = ReactDOM.createRoot(document.getElementById('root'));

// 创建jsx结构
const list = [
  { id: 1, name: '王鑫航', salary: 2e4 },
  { id: 2, name: '刘海涛', salary: 8e4 },
  { id: 3, name: '王亦凡', salary: 1e5 }
]
/**
 * 控制jsx样式：
 * 1. 行内样式
 *    语法：style={{css属性名1:css属性值1, css属性名2:css属性值2, ...}}
 *    注意：横杠分割的css属性名要写成驼峰形式
 * 2. 类名控制（推荐）
 */
const listBox = (
  <>
    {/* <div style={{ border: '8px solid red', padding: '10px', fontSize: '80px' }}> */}
    <div className='box'>
      {
        list.map((item, i) =>
          <p key={item.id}>序号：{i + 1}，姓名：{item.name}, 期望薪资：{item.salary}</p>
        )
      }
    </div>
  </>
)
// 3. 使用root根实例绘制用户界面（视图html）
root.render(
  listBox
);