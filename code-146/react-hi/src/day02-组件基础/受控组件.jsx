import { Component } from "react";

class App extends Component {
  /**
   * 受控组件：value值受到了react==状态控制==的表单元素
   * 类似vue的v-model双向绑定：<input type="text" :value="data变量" @input="callback" />
   * 使用场景：表单开发，获取表单输入框的值
   */
  // 1. 定义变量
  state = {
    val: "你好，受控",
  };

  handlerChange = (e) => {
    // console.log("事件对象：", e);
    // 1. 视图变化：输入框输入的最新值
    console.log("输入框的值：", e.target.value);
    // 2. 视图没刷新？=》调用setState方法改val变量
    this.setState({
      val: e.target.value,
    });
  };

  render() {
    return (
      <div>
        <h1>受控组件</h1>
        <hr />
        <p>
          {/* 
           一旦输入框： value={this.state.val} =》受控了=》受react的state状态控制
          */}
          {/* 2. 动态绑定value值和添加一个onChange事件*/}
          <input
            type="text"
            value={this.state.val}
            onChange={this.handlerChange}
          />
        </p>
      </div>
    );
  }
}
export default App;
