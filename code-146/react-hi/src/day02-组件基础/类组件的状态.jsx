import { Component } from "react";

class TestState extends Component {
  /**
   * **内容**：
  + 状态`state`，即数据，是组件内部的**私有数据**，只能在组件内部使用
  + 状态 state 的值是**对象**，表示一个组件中可以有多个数据
  + 通过 `this.state.xxx` 来获取状态
   * @returns 
   */
  // 类比：vue在script的 data() {  return {data变量}  }
  // 1. 定义data变量（状态）
  state = {
    count: 0,
    obj: {
      age: 18,
    },
    list: [1, 2, 3],
  };

  add = () => {
    /**
     * vue修改数据思想：== 数据可变 == =》 可以直接修改之前的值
     * vue修改数据：this.变量名 = 新值
     *
     * react修改数据思想：== 数据不可变 == => 不能直接修改之前的值
     * 使用一个方法：this.setState({
     *    变量名1:最新修改的值1,
     *    变量名2:最新修改的值2,
     *    ...
     * })
     * 说明❓，修改数据口诀：== 新值 == 覆盖老值
     */
    // this.state.count = 100;
    // this.state.count++ 等价于 this.state.count = this.state.count +1
    console.log("组件实例：", this);
    // this.state.list.push(Math.random());
    let newObj = {
      ...this.state.obj,
      age: 20,
    };
    let newList = [...this.state.list, Math.random()];
    this.setState({
      count: this.state.count + 1,
      obj: newObj,
      list: newList,
    });
  };

  render() {
    return (
      <div>
        <h1>类组件的状态</h1>
        <hr />
        {/* 2. 通过this.state.变量名 */}
        <ul>
          <li>{this.state.count}</li>
          <li>{this.state.obj.age}</li>
          <li>
            <button onClick={this.add}>add</button>
          </li>
          <li>{this.state.list.join(",")}</li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
          <li></li>
        </ul>
      </div>
    );
  }
}

export default TestState;
