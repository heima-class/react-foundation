/**
 * 1. 函数组件
 * 函数组件：使用JS的函数或者箭头函数创建的组件
+ 使用 JS 的函数(或箭头函数)创建的组件，叫做`函数组件`	
+ 约定1：**函数名称必须以大写字母开头**（大驼峰命名），React 据此区分组件和普通的 HTML标签
+ 约定2：**函数组件必须有返回值**，表示该组件的 UI 结构；如果不需要渲染任何内容，则返回 `null`

使用：函数名就是标签名

回顾：vue框架里组件格式.vue =》1. template（html） 2. script(js) 3. style(css)
 */

/**
 * vue框架：.vue格式组件
 * 1. html => <template>html结构</template>
 * 2. js => <script>
 *   export default {
 *    data() {
 *     return {}
 *    },
 *    created() {
 *    },
 *    methods:{
 *      fn1() {}
 *    }
 * }
 *   </script>
 * 3. css 样式
 */

// 3. css样式
import './index.css'
const MyFn = () => {
  // 2. js逻辑
  const a = 100
  // 1. 返回组件的视图html
  return (
    <div className="fn">
      <h1>我的第一个函数组件:{a}</h1>
    </div>
  )
}

export default MyFn
