import { Component, createRef } from "react";

class Son extends Component {
  test = () => {
    console.log(this);
  };
  render() {
    return (
      <div>
        <h2>儿子组件</h2>
      </div>
    );
  }
}

class App extends Component {
  /**
   * 非受控组件：
   * 类似vue模板中使用的ref
   * 1. 可以获取html元素的dom对象
   * 2. 可以获取类组件的实例
   * 使用步骤：
   * 1. 导入createRef方法并创建一个ref对象
   * 2. 元素上通过：ref={ref对象}
   * 3. 通过：this.ref对象.current (dom对象 | 组件实例)
   */
  // 1. 创建h1元素的ref对象
  h1 = createRef();

  son = createRef();

  getDom = () => {
    console.log("h1", this.h1);
    this.h1.current.style.border = "10px solid red";
    console.log("子组件实例：", this.son.current);
    this.son.current.test();
  };

  render() {
    return (
      <div>
        {/* 2. 绑定ref对象 */}
        <h1 ref={this.h1}>非受控组件</h1>

        <p>
          <button onClick={this.getDom}>获取h1的dom</button>
        </p>
        <hr />
        <Son ref={this.son} />
      </div>
    );
  }
}
export default App;
