import { Component } from "react";

// 函数子组件
const Child = () => {
  const handerClick = () => {
    console.log(456);
  };
  return (
    <div>
      <h2 onClick={handerClick}>子组件</h2>
    </div>
  );
};

class App extends Component {
  /**
   * 事件绑定：
   * - React注册事件与DOM的事件语法非常像
- 语法`on+事件名 =｛回调函数｝` 比如`onClick={this.handleClick}`
- 注意：*React事件采用驼峰命名法*，比如`onMouseEnter`, `onClick`
  注意：
   1. 绑定的事件回调函数，不能加圆括号，例如：onClick={this.handlerClick()}=》错误的
   如果需要传参（口诀）：函数套函数

   事件对象：
   1. 默认没有传参，回调函数的第一个形参就是事件对象
   2. 传参了，通过第一层函数的第一个形参就是事件对象
   * @returns 
   */
  state = {
    count: 0,
  };

  handlerClick(data, e) {
    // console.log("接收参数：", data, e);
    console.log("组件实例：", this.state.count);
  }

  // 定义为箭头函数方法=>避免this指向问题
  handlerClick2 = () => {
    console.log("组件实例：", this);
  };

  render() {
    return (
      <div>
        <h1>事件绑定</h1>
        <hr />
        <p>
          {/* 解决this指向问题：
          1. 使用bind
          2. 直接把事件回调函数定义为箭头函数（推荐）
          */}
          <button onClick={this.handlerClick.bind(this)}>点击1</button>
          <button onClick={this.handlerClick2}>点击3</button>

          <br />
          <button
            onClick={(event) => {
              console.log(0, event, this);
              this.handlerClick(1000, event);
            }}
          >
            点击2
          </button>
        </p>
        <Child />
      </div>
    );
  }
}
export default App;
