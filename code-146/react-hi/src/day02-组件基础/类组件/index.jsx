import { Component } from 'react'
import './index.css'
/**
 * 2. 类组件
 * - 使用 ES6 的 class 创建的组件，叫做类（class）组件
- 约定1：**类名称也必须以大写字母开头**
- 约定2：类组件应该继承 React.Component 父类，从而使用父类中提供的方法或属性 
- 约定3：类组件必须提供 render 方法
- 约定4：render 方法必须有返回值，表示该组件的 UI 结构

使用：类名就是标签名
 */
class MyClassCp extends Component {
  // 2. js逻辑

  render() {
    // 1. 返回组件的视图html
    return (
      <div className="cla">
        <h1>我的第一个类组件</h1>
      </div>
    )
  }
}

export default MyClassCp
