import './App.css'

import { useSelector, useDispatch } from 'react-redux'
import { add, changeObj, asyncAction } from './store/modules/counter'
import { useEffect } from 'react'
import { getListAction } from './store/modules/news'
/**
 * 1. 获取store中的状态数据
 * 语法：const 状态数据 = useSelector(state => state.模块名)
 *
 * 2. 修改store中的状态数据
 * 步骤：
 * // 1. 调用 useDispatch hook，拿到 dispatch 函数
  const dispatch = useDispatch()
  // 2. 调用 dispatch 传入 action，来分发动作
  dispatch(action(payload))
 */
function App() {
  const dispatch = useDispatch()

  const { count, obj, list } = useSelector((state) => {
    // console.log('store所有数据：', state)
    return state.counter
  })

  const newList = useSelector((state) => state.news.list)

  // 组件挂载
  useEffect(() => {
    dispatch(getListAction())
  }, [dispatch])

  const addCount = () => {
    // dispatch(add())
    // dispatch(changeObj(20))
    dispatch(asyncAction(1000))
  }
  return (
    <div className="App">
      <ul>
        <li>{count}</li>
        <li>
          <button onClick={addCount}>add</button>
        </li>
        <li>
          {obj.name}-{obj.age}
        </li>
        <li>{list.join(',')}</li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
        <li></li>
      </ul>
      <hr />
      <ul>
        {newList.map((item) => (
          <li key={item.id}>{item.title}</li>
        ))}
      </ul>
    </div>
  )
}

export default App
