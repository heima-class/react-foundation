/**
 * 创建store
 */
import { configureStore } from '@reduxjs/toolkit'
// 1. 导入子模块
import counter from './modules/counter'
import login from './modules/login'
import news from './modules/news'

export default configureStore({
  reducer: {
    // 2. 注册子模块
    counter,
    login,
    news
  }
})