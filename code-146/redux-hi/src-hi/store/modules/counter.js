import { createSlice } from '@reduxjs/toolkit'

export const counter = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'counter',
  // 1. 定义变量(状态数据)
  initialState: {
    count: 0,
    obj: {
      name: '小红',
      age: 18
    },
    list: [1, 2, 3]
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // 修改count：自增
    /**
     * 
     * @param {*} state 所有变量
     * @param {*} action 获取调用add方法的参数payload
     */
    add (state, action) {
      state.count++
    },
    changeObj (state, action) {
      console.log('obj-action:', action)
      state.obj.age = action.payload
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { add, changeObj } = counter.actions

// 3. 异步action
export const asyncAction = (payload) => {
  /**
   * payload 调用asyncAction传入的参数
   * dispatch 调用reducer函数修改变量
   * getState 获取store中所有数据
   */
  return async (dispatch, getState) => {
    console.log('store数据：', getState(), payload)
    setTimeout(() => {
      dispatch(add())

    }, 3000);
  }
}

// 导出reducer(创建store使用)
export default counter.reducer