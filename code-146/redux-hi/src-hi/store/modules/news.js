import { createSlice } from '@reduxjs/toolkit'
import request from '../../utils/request'

export const news = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'news',
  // 1. 定义变量(状态数据)
  initialState: {
    list: []
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    setList (state, action) {
      state.list = action.payload
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { setList } = news.actions

// 3. 异步action
/**
 * 需求：
 * 1. 获取http://ajax-api.itheima.net/api/news数据并存储到redux
 * 2. 在App.jsx中获取新闻列表渲染
 */
export const getListAction = (payload) => {
  return async (dispatch, getState) => {
    const { data } = await request.get('/api/news')
    console.log(data.data)
    dispatch(setList(data.data))
  }
}

// 导出reducer(创建store使用)
export default news.reducer