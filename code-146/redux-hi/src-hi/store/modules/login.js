import { createSlice } from '@reduxjs/toolkit'

export const login = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'login',
  // 1. 定义变量(状态数据)
  initialState: {
    token: ''
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    add (state, action) {
      state.count++
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { add } = login.actions

// 3. 异步action
export const asyncAction = (payload) => {
  return async (dispatch, getState) => {
    dispatch(add())
  }
}

// 导出reducer(创建store使用)
export default login.reducer