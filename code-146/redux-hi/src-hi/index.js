import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

// 1. 导入Provider组件
import { Provider } from 'react-redux'
// 2. 导入reudx的store实例
import store from './store'

console.log('store:', store)

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  // 3. 使用Provider组件给react应用提供store中共享数据
  <Provider store={store}>
    <App />
  </Provider>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
