import { createRoot } from 'react-dom/client'

// 全局
import './styles/base.css'
import './styles/index.css'
import App from './App'

import { Provider } from 'react-redux'
import store from './store'


createRoot(document.getElementById('root')).render(
  // 集成redux的store
  <Provider store={store}>
    <App></App>
  </Provider>
)