import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addTodo } from '../../store/modules/todo'
function Head() {
  // 2. 新增任务
  const dispatch = useDispatch()

  const [text, setText] = useState('')

  const onAddTodo = (e) => {
    if (e.key !== 'Enter') return
    if (text.trim() === '') return

    dispatch(addTodo(text))

    setText('')
  }
  return (
    <header className="header">
      <h1>待办事项</h1>
      <input
        className="new-todo"
        placeholder="输入任务名"
        value={text}
        onChange={(e) => setText(e.target.value)}
        onKeyDown={onAddTodo}
      />
    </header>
  )
}

export default Head
