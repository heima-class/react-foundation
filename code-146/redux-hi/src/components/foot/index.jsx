import { useSelector } from 'react-redux'

function Foot() {
  // 统计未完成任务数量?
  // 一个任务通过done：true 完成 | false 未完成
  const unDone = useSelector((state) => {
    return state.todo.list.filter((item) => item.done === false).length
  })
  return (
    <footer className="footer">
      <span className="todo-count">
        <strong>{unDone}</strong> 未完成
      </span>
    </footer>
  )
}

export default Foot
