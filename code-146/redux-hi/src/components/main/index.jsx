import { useDispatch, useSelector } from 'react-redux'
import { changeDone, delList } from '../../store/modules/todo'

function Main() {
  // 1. 获取redux中list
  const { list } = useSelector((state) => state.todo)
  // 获取dispatch
  const dispatch = useDispatch()

  return (
    <section className="main">
      <input id="toggle-all" className="toggle-all" type="checkbox" />
      <label htmlFor="toggle-all" />
      {/* 待办事项列表 */}
      <ul className="todo-list">
        {list.map((item) => (
          <li className={item.done ? 'completed' : ''} key={item.id}>
            <div className="view">
              {/* 选择框：已完成 | 未完成 */}
              <input
                className="toggle"
                type="checkbox"
                checked={item.done}
                onChange={(e) => {
                  dispatch(changeDone(item.id))
                }}
              />
              {/* 任务名 */}
              <label>{item.text}</label>
              {/* 删除按钮 */}
              <button
                className="destroy"
                onClick={() => {
                  // 执行任务删除？任务数据在哪里存的？=》redux
                  dispatch(delList(item.id))
                }}
              ></button>
            </div>
          </li>
        ))}
      </ul>
    </section>
  )
}

export default Main
