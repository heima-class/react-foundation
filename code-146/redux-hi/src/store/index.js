import { configureStore } from '@reduxjs/toolkit'
import todo from './modules/todo'

export default configureStore({
  reducer: {
    todo
  }
})