import { createSlice } from '@reduxjs/toolkit'

export const todo = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'todo',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [
      { id: 1, text: '吃饭', done: true },
      { id: 2, text: '学习', done: false },
      { id: 3, text: '睡觉', done: true },
    ]
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action.payload 删除任务的ID
    delList (state, action) {
      state.list = state.list.filter(item => item.id !== action.payload)
    },
    /**
     * 修改任务选中状态
     * @param {*} state 
     * @param {*} action.payload 任务ID 
     */
    changeDone (state, action) {
      /**
       * 1. 找到当前修改的任务
       * 2. 执行任务状态修改 => 和上次相反
       */
      const currTask = state.list.find(item => item.id === action.payload)
      currTask.done = !currTask.done
    },
    addTodo (state, action) {
      state.list.unshift({
        id: Date.now(),
        text: action.payload,
        done: false
      })
    }

  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { delList, changeDone, addTodo } = todo.actions

// 3. 异步action
// export const asyncAction = (payload) => {
//   return async (dispatch, getState) => {
//     dispatch(add())
//   }
// }

// 导出reducer(创建store使用)
export default todo.reducer