#  Redux基础

+ [ ] redux基本介绍
+ [ ] redux核心用法
+ [ ] redux执行过程

## 为什么要用 Redux

<img src="assets/image-20220106005836131.png" alt="image-20220106005836131" style="zoom: 50%;" />

**目标：**能够说出为什么需要使用redux

**内容：**

[redux 中文文档](http://cn.redux.js.org/)

[redux 英文文档](https://redux.js.org/)

> Redux 是 React 中最常用的状态管理工具（状态容器）

React 只是 DOM 的一个抽象层（UI 库），并不是 Web 应用的完整解决方案。因此 React 在涉及到数据的处理以及组件之间的通信时会比较复杂

> 对于大型的复杂应用来说，这两方面恰恰是最关键的。因此，只用 React，写大型应用比较吃力。

背景介绍：

- 2014 年 Facebook 提出了 Flux 架构的概念（前端状态管理的概念），引发了很多的实现
- 2015 年，Redux 出现，将 Flux 与*函数式编程*结合一起，很短时间内就成为了最热门的前端架构
- Flux 是最早的前端的状态管理工具，它提供了状态管理的思想，也提供对应的实现
- 除了 Flux、Redux 之外，还有：Mobx 等状态管理工具

问题：为什么要用 Redux?

<img src="assets/with-redux.png" style="width: 800px" />

- 主要的区别：**组件之间的通讯问题**
- 不使用 Redux (图左边) ：
  - 只能使用父子组件通讯、状态提升等 React 自带机制 
  - 处理远房亲戚(非父子)关系的组件通讯时乏力
  - 组件之间的数据流混乱，出现 Bug 时难定位
- 使用 Redux (图右边)：
  - **集中式存储和管理应用的状态**
  - 处理组件通讯问题时，**无视组件之间的层级关系** 
  - 简化大型复杂应用中组件之间的通讯问题
  - 数据流清晰，易于定位 Bug

## Redux 开发环境准备

**目标：**能够在react项目中准备redux开发环境

**内容：**

使用 React CLI 来创建项目，并安装 Redux 包即可：

1. 创建 React 项目：`npx create-react-app redux-basic`
2. 启动项目：`npm start`
3. 安装 Redux依赖包：`npm i @reduxjs/toolkit react-redux`

## Redux-创建store和模块

**目标：**为项目创建store，维护全局状态

回顾：

vuex核心三要素？

1. state  定义变量
2. mutations（同步） 修改变量的方法
3. actions （异步）获取后台数据，调用mutations中定义的方法存储数据



**内容：**

> store：仓库，Redux 的核心，遵循单一store原则
>
> 核心概念：
>
> 1. initialState 定义变量
> 2. reducers （同步） 修改变量的方法
> 3. thunk actions （异步）获取后台数据，调用reducers中定义的方法存储数据
>
> 使用：
>
> 组件中可以dispatch触发reducer函数更新状态

**核心代码：**

`src/store/index.js`

```js
import { configureStore } from '@reduxjs/toolkit'
// 1. 导入子模块
import counter from './modules/counter'

export default configureStore({
  reducer: {
    // 2. 注册子模块
    counter
  }
})
```

`src/store/modules/counter.js`

```jsx
import { createSlice } from "@reduxjs/toolkit"

export const counter = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action的前缀,避免冲突
  name: 'counter',
  // 1. 定义变量
  initialState: {
    count: 1
  },
  // 2. 修改变量
  reducers: {
    // action函数
    add (state, action) {
      // console.log(state, action)
      state.count++
    }
  },
})

// 导出actions（组件中可以dispatch触发reducer函数更新状态）
export const { add } = counter.actions

// 导出reducer（创建store使用）
export default counter.reducer
```

## Redux-react项目集成

**目标**：使用react-redux库在react中使用redux管理状态

**内容**：

[react-redux 文档](https://react-redux.js.org/introduction/getting-started)

react-redux 的使用分为两大步：1 全局配置（只需要配置一次） 2 组件接入（获取状态或修改状态）

先看全局配置：

**步骤**：

1. 安装 react-redux：` npm i react-redux`
2. 从 react-redux 中导入 Provider 组件
3. 导入创建好的 redux 仓库
4. 使用 Provider 包裹App根组件
5. 将导入的 store 设置为 Provider 的 store 属性值

**核心代码**：

src/index.js 中：

```js
// 1. 导入 Provider 组件
import { Provider } from 'react-redux'
// 2. 导入创建好的 store
import store from './store'

ReactDOM.render(
  // 3. 集成redux
  <Provider store={store}>
    <App />
  </Provider>,
  document.querySelector('#root')
)
```

## Redux-获取状态

**目标**：能够使用 useSelector hook 获取redux中共享的状态

Vuex：store.state | mapState

**内容**：

- `useSelector`：获取 Redux 提供的状态数据
- 参数：selector 函数，用于从 Redux 状态中筛选出需要的状态数据并返回
- 返回值：筛选出的状态

```js
import { useSelector } from 'react-redux'

// 获取子模块状态
const 状态数据 = useSelector(state => state.模块名)
```

**核心代码**：

App.js 中：

```jsx
import { useSelector } from 'react-redux'

const App = () => {
 const { count } = useSelector(state => state.counter)  
  return (
  	<div>
    	<h1>计数器：{count}</h1>
      <button>数值增加</button>
			<button>数值减少</button>
    </div>
  )
}
```

## Redux-修改状态

**目标**：能够使用 useDispatch hook 修改redux中共享的状态

Vuex：store.commit('mutations方法名',  payload)

**内容**：

- `useDispatch`：拿到 dispatch 函数，分发 action，修改 redux 中的状态数据
- 语法：

```js
import { useDispatch } from 'react-redux'

// 1. 调用 useDispatch hook，拿到 dispatch 函数
const dispatch = useDispatch()

// 2. 调用 dispatch 传入 action，来分发动作
dispatch(action(payload))
```

**核心代码**

App.js 中：

```jsx
import { useDispatch } from 'react-redux'
import { add } from './store/modules/counter'


const App = () => {
  const dispatch = useDispatch()
  
  return (
  	<div>
    	<h1>计数器：{count}</h1>
      {/* 调用 dispatch 分发 action */}
      <button onClick={() => dispatch(add())}>数值增加</button>
    </div>
  )
}
```

## Redux-异步action

**目标：**能够掌握异步action创建和使用

**内容：**

>异步action：**用来发送请求获取后台数据并存储到redux**

**语法：**

```js
const thunkAction = (payload) => {
  // 注意：此处返回的是一个函数，返回的函数有两个参数：
	// 第一个参数：dispatch 函数，用来分发 action
  // 第二个参数：getState 函数，用来获取 redux 状态
  return (dispatch, getState) => {
    setTimeout(() => {
      // 执行异步操作
      // 在异步操作成功后，可以继续分发对象形式的 action 来更新状态
    }, 1000)
  }
}
```

**核心代码**：

`src/store/modules/counter.js`

1. 创建

```diff
import { createSlice } from "@reduxjs/toolkit"

export const counter = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action的前缀,避免冲突
  name: 'counter',
  // 状态的初始值
  initialState: {
    count: 1,
+    data:[]
  },
  // 1. 定义reducer函数更新状态
  reducers: {
    add (state, action) {
      // console.log(state, action)
      state.count++
    },
+    setData (state, action) {
+      state.data = action.payload
+    }
  },
})

// 导出actions（组件中可以dispatch触发reducer函数更新状态）
+ export const { add, setData } = counter.actions

- // 异步action
+ export const getDataAction = (payload) => {
+  return async (dispatch, getState) => {
+    // 模拟发请求
+    setTimeout(() => {
+			 const res = [1,2,3]
+      dispatch(setData(res))
+    }, 2000)
+  }
+}

// 导出reducer（创建store使用）
export default counter.reducer
```

2. 使用

```js
import { useDispatch, useSelector } from 'react-redux'
// 1. 导入异步action
import { getDataAction } from './store/modules/counter'

  useEffect(() => {
    // 2. 组件挂载执行action
    const dispatch = useDispatch()
    dispatch(getDataAction())
  }, [dispatch])
```



## Redux-执行过程

**目标**：能够说出redux数据流动过程

**内容**：

<img src="./assets/ReduxDataFlow.gif" style="width: 800px" />

**总结**：

- 任何一个组件都可以直接接入 Redux，也就是可以直接：1 修改 Redux 状态 2 接收 Redux 状态
- 并且，只要 Redux 中的状态改变了，所有接收该状态的组件都会收到通知，也就是可以获取到最新的 Redux 状态
- 这样的话，两个组件不管隔得多远，都可以**直接通讯**了

## Redux应用-代码结构

**目标**：能够组织redux的代码结构

**内容**：

可以按照以下结构，来组织 Redux 的代码：

```html
/store        --- 在 src 目录中创建，用于存放 Redux 相关的代码
  /modules    --- 存放所有的 reducer子模块
  index.js    --- redux 的入口文件，用来创建 store
```
## Redux应用-开发方式

**目标**：能够知道什么状态可以放在redux中管理

**内容**：

不同状态的处理方式：

1. **将所有的状态全部放到 redux 中**，由 redux 管理
2. **将全局共享状态数据放在 redux 中**，其他数据可以放在组件中，比如：
   - 如果一个状态，只在某个组件中使用（比如，表单项的值），推荐：放在组件中
   - 需要放到 redux 中的状态：
     1. 在多个组件中都要使用的数据【涉及组件通讯】

## 综合案例-待办事项

### 1. 案例结构搭建

**目标**：能够根据模板搭建案例结构

**内容**：使用准备好的模板内容，搭建项目，并分析案例的中组件的层级结构

```html
App
	TodoHeader
	TodoMain
	TodoFooter
```

### 2. 配置Redux基本结构

**目标**：能够在todomvc案例中配置Redux

**步骤**：

1. 安装 Redux依赖包：`npm i @reduxjs/toolkit`
2. 在 src 目录中创建 store 文件夹
3. 在 store 目录中创建modules目录以及 index.js 文件
4. 在 modules 目录中新建 todos.js 
7. 在 store/index.js 中，创建 store 然后导出

**核心代码**：

store/modules/todos.js 中：

```js
import { createSlice } from '@reduxjs/toolkit'

export const todo = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'todo',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [
      { id: 1, text: '吃饭', done: true },
      { id: 2, text: '学习', done: false },
      { id: 3, text: '睡觉', done: true }
    ]
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const {  } = todo.actions


// 导出reducer(创建store使用)
export default todo.reducer
```

store/index.js 中：

```js
import { configureStore } from '@reduxjs/toolkit'
import todo from './modules/todo'

export default configureStore({
  reducer: {
    todo
  }
})
```

### 3. 配置React-Redux

**目标**：能够在 todomvc 案例中配置 react-redux

**步骤**：

1. 安装 react-redux：`npm i react-redux`
2. 在 src/index.js 中，导入 Provider 组件
3. 在 src/index.js 中，导入创建好的 store
4. 使用 Provider 包裹 App 组件，并设置其 store 属性

**核心代码**：

src/index.js 中：

```js
import { createRoot } from 'react-dom/client'

import './styles/base.css'
import './styles/index.css'
import App from './App'

import { Provider } from 'react-redux'
import store from './store'


createRoot(document.getElementById('root')).render(
  <Provider store={store}>
    <App></App>
  </Provider>
)
```

### 4. 渲染任务列表

**目标**：能够渲染任务列表

**步骤**：

1. 在 TodoMain 组件中导入 useSelector hook
2. 调用 useSelector 拿到 todos 状态，也就是任务列表数据
3. 遍历任务列表数据渲染

**核心代码**：

TodoMain.js 中：

```jsx
import { useSelector } from 'react-redux'

export const TodoMain = () => {
  const { list } = useSelector(state => state.todo)

  return (
    // ...
      <ul className="todo-list">
        {list.map((item) => (
          <li key={item.id} className={item.done ? 'completed' : ''}>
            <div className="view">
              <input className="toggle" type="checkbox" />
              <label>{item.text}</label>
              <button className="destroy"></button>
            </div>
          </li>
        ))}
      </ul>
  )
}
```

### 5. 渲染未完成任务数量

**目标**：能够渲染未完成任务数量

**分析说明**：

问题❓：实现该功能，是添加一个新的状态，还是用当前已有的状态？

回答：看一下要用到的这个数据，能不能直接根据现有的状态得到，如果能直接用现有的数据即可；否则，就要创建新的状态了

比如，现在要用的未完成任务数量，可以直接从 todos 任务列表数据中过滤得到，所以，直接用当前数据即可

**步骤**：

1. 在 TodoFooter 组件中导入 useSelector hook
2. 调用 useSelector 拿到 todos 状态，也就是任务列表数据
3. 根据任务列表数据，过滤出未完成任务并拿到其长度，然后渲染

**核心代码**：

TodoFooter.js 中：

```jsx
import { useSelector } from 'react-redux'

export const TodoFooter = () => {
	const leftCount = useSelector(
    state => state.todo.list.filter(item => !item.done).length
  )

  return (
		// ...
    <span className="todo-count">
      <strong>{leftCount}</strong> item left
    </span>
  )
}
```

### 6. 删除任务

**目标**：能够实现删除任务功能

**步骤**：

1. 给删除按钮绑定点击事件
5. 在 modules/todo.js 中，定义action函数
5. 在组件中分发删除action函数

**核心代码**：

TodoMain.js 中：

```jsx
import { useDispatch } from 'react-redux'
import { del } from '../../store/modules/todo'

export const TodoMain = () => {
  const dispatch = useDispatch()

  return (
    // ...
		<button className="destroy" onClick={() => dispatch(del(item.id))}></button>
  )
}
```

modules/todo.js 中：

```diff
export const todo = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'todo',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [
      { id: 1, text: '吃饭', done: true },
      { id: 2, text: '学习', done: false },
      { id: 3, text: '睡觉', done: true }
    ]
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
+    del (state, action) {
+      state.list = state.list.filter(item => item.id !== action.payload)
+    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
+ export const { del } = todo.actions
```

### 7. 切换任务完成状态

**目标**：能够实现切换任务完成状态

**步骤**：

1. 为 TodoMain 组件中的 checkbox 添加 checked 值为：item.done 并为其绑定 change 事件
2. 在 change 事件中分发切换任务完成状态的 action函数
4. 在 modules/todo.js 文件，创建切换任务的 action 函数并导出



**核心代码**：

TodoMain.js 中：

```jsx
import { toggleTodo } from '../../store/modules/todo'

export const TodoMain = () => {
  return (
    // ...
    <input
      className="toggle"
      type="checkbox"
      checked={item.done}
      onChange={() => dispatch(toggleTodo(item.id))}
      />
  )
}
```

modules/todo.js 中：

```diff
import { createSlice } from '@reduxjs/toolkit'

export const todo = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'todo',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [
      { id: 1, text: '吃饭', done: true },
      { id: 2, text: '学习', done: false },
      { id: 3, text: '睡觉', done: true }
    ]
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
    del (state, action) {
      console.log('action:', action)
      state.list = state.list.filter(item => item.id !== action.payload)
    },
+    toggleTodo (state, action) {
+      const task = state.list.find(item => item.id === action.payload)
+      task.done = !task.done
+    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
+ export const { del, toggleTodo } = todo.actions
```

### 8. 添加任务(作业)

**目标**：能够实现添加任务

**分析说明**：

问题❓：控制文本框的状态，应该放在 redux 中，还是放在组件中？

回答：组件中

**步骤**：

1. 在 TodoHeader 组件中通过受控组件获取文本框的值
2. 给 input 绑定 keyDown 事件，在事件处理程序中判断按键是不是回车
3. 如果不是，直接 return 不执行添加操作
4. 如果是，分发添加任务的 action
5. 新增添加任务的 action函数
7. 对添加任务功能进行非空校验和清空文本框的操作

**核心代码**：

TodoHeader.js 中：

```jsx
import { useState } from 'react'
import { useDispatch } from 'react-redux'
import { addTodo } from '../../store/modules/todo'

export const TodoHeader = () => {
  const dispatch = useDispatch()
  const [text, setText] = useState('')

  const onAddTodo = e => {
    if (e.key !== 'Enter') return
    if (text.trim() === '') return

    dispatch(addTodo(text))

    setText('')
  }

  return (
    <header className="header">
      <h1>todos</h1>
      <input
        className="new-todo"
        placeholder="What needs to be done?"
        autoFocus
        value={text}
        onChange={e => setText(e.target.value)}
        onKeyDown={onAddTodo}
      />
    </header>
  )
}
```

modules/todo.js 中：

```diff
import { createSlice } from '@reduxjs/toolkit'

export const todo = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'todo',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [
      { id: 1, text: '吃饭', done: true },
      { id: 2, text: '学习', done: false },
      { id: 3, text: '睡觉', done: true }
    ]
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
+    addTodo (state, action) {
+      state.list.push({
+        id: Date.now(),
+        text: action.payload,
+        done: false
+      })
+    }
+  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
+ export const { addTodo } = todo.actions
```



## 第六天



