# React 路由

+ [ ] React 路由介绍
+ [ ] 路由的基本使用
+ [ ] 路由的执行过程
+ [ ] 路由的常用操作

## React 路由介绍

> 现代的前端应用大多都是 SPA（单页应用程序），也就是只有一个 HTML 页面的应用程序。因为它的用户体验更好、对服务器的压力更小，所以更受欢迎。为了**有效的使用单个页面来管理原来多页面的功能，前端路由应运而生**。前端路由的功能：让用户从一个视图（页面）导航到另一个视图（页面）

- 前端路由是一套**映射规则**，在React中，是 *URL路径* 与 *组件* 的对应关系 
- 使用 React 路由简单来说就是：配置路径和组件（配对）

<img src="assets/路由是一套规则1-16297300974362.png" style="width: 800px" />
<img src="assets/路由是一套规则2-16297300974363.png" style="width: 800px" />

## 路由基本使用

**目标**：能够使用 react 路由切换页面

**内容**：

- 2021.11月初，react-router 更新到了 v6 版本。
- 注意：v6 版本相比 v5 版本有破坏性更新（使用方式不同了）！
- [v6 文档-默认版本](https://reactrouter.com/)
- [v5 文档](https://v5.reactrouter.com/)
- [v5-v6对比](https://qdmana.com/2022/02/202202050641334596.html)

**步骤**：

1. 安装：`npm i react-router-dom`
2. 导入路由的三个核心组件：BrowserRouter(HashRouter) / Routes / Route
3. 使用 BrowserRouter(HashRouter) 组件包裹整个应用
4. 使用 Link 组件作为导航菜单（路由入口）
5. 使用 Route 组件配置路由规则和要展示的组件（路由出口）

**核心代码**：

```jsx
import './App.css'
// 路由依赖
import {
  BrowserRouter,
  // HashRouter,
  Routes,
  Route,
  Link,
} from 'react-router-dom'
import Home from './pages/home'
import News from './pages/news'
import About from './pages/about'

function App() {
  return (
    <BrowserRouter>
      {/* 导航 */}
      <div className="nav">
        <Link to="/home">首页</Link>
        <Link to="/news">新闻</Link>
        <Link to="/about">关于我们</Link>
      </div>
      {/* 配置路由 */}
      <Routes>
        <Route path="/home" element={<Home />} />
        <Route path="/news" element={<News />} />
        <Route path="/about" element={<About />} />
      </Routes>
    </BrowserRouter>
  )
}

export default App
```

## 路由的两种模式

**目标**：能够知道 react 路由有两种模式

**内容**：

> 两种常用 Router：`HashRouter` 和 `BrowserRouter`  

1. HashRouter：使用 URL 的哈希值实现（http://localhost:3000/#/first）
   - 原理：监听 window 的 `hashchange` 事件来实现的

2. BrowserRouter：使用 H5 的 history.pushState() API 实现（http://localhost:3000/first）
   - 原理：监听 window 的 `popstate` 事件来实现的

```js
import { BrowserRouter, HashRouter} from 'react-router-dom'

const App = () => {
  return (
    // hash模式（history模式使用BrowserRouter包裹）
    <HashRouter>
			// 配置路由规则...
    </HashRouter>
 	)
}
```

## Route组件用法

**目标**：能够使用 Route 组件配置路由规则

**内容**：

`Route` 组件：用来配置路由规则

- `path` 属性，指定路由规则
- `element` 属性，指定要渲染的组件

注意❓：Route组件配置路由**必须使用Routes组件包裹**

```jsx
<Routes>
  <Route path="/search" element={Search} />
</Routes>
```

## Link组件用法

**目标**：能够使用 Link 组件实现路由跳转

**内容**：

`Link` 组件：用于指定导航链接，会渲染成 a 标签

+ `to` 属性，将来会渲染成 a 标签的 href 属性

```jsx
<Link to="/first">页面一</Link>

// 渲染为：
<a href="/first">页面一</a>
```

## useRoutes配置路由

**目标**：能够使用 useRoutes配置路由

**内容**：v6 版本中提供了自定义 hooks `useRoutes` 让路由的配置更加方便灵活

**步骤：**

1. 在根组件App.js中准备路由规则数组
2. 使用useRoutes函数生成规则元素
3. 在组件中渲染生成规则元素
4. 入口index.js中使用Router组件包裹根组件

`App.jsx`

```js
import { useRoutes } from 'react-router-dom'
import Home from './pages/home'
import News from './pages/news'
import About from './pages/about'

function App() {
  // 1. 路由配置规则
  const routeConfig = [
    {
      path: '/',
      element: <Home />,
    },
    {
      path: '/news',
      element: <News />,
    },
    {
      path: '/about',
      element: <About />
    },
  ]
  // 2. 生成规则配置元素
  const element = useRoutes(routeConfig)
  return (
    <div className="app">
      {/* 导航 */}
      <div className="nav">
        <Link to="/">首页</Link>
        <Link to="/news">新闻</Link>
        <Link to="/about">关于我们</Link>
      </div>
      {/* 3. 配置路由 */}
      {element}
    </div>
  )
}
```

`index.js`

```diff
import React from 'react'
import ReactDOM from 'react-dom/client'
import './index.css'
import App from './App'
import reportWebVitals from './reportWebVitals'
+ import { BrowserRouter } from 'react-router-dom'

const root = ReactDOM.createRoot(document.getElementById('root'))
root.render(
+ <BrowserRouter>
    <App />
+ </BrowserRouter>
)

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals()

```

说明❓：必须使用Router组件包裹**使用useRoutes配置路由的组件**

## 嵌套路由配置

**目标**：能够使用 useRoutes配置子路由

**内容**：useRoutes配置子路由

**步骤：**

1. 在根组件App.js中添加子路由规则
2. 在父路由组件使用Outlet组件占位

`App.js`

```diff
import { useRoutes } from 'react-router-dom'
import Home from './pages/home'
import News from './pages/news'
import About from './pages/about'
+ import Contact from './pages/about/contact'

function App() {
  // 1. 路由配置规则
  const routeConfig = [
    {
      path: '/',
      element: <Home />,
    },
    {
      path: '/news',
      element: <News />,
    },
    {
      path: '/about',
      element: <About />,
+     children: [{ path: '/about/contact', element: <Contact /> }],
    },
  ]
  // 2. 生成规则配置元素
  const element = useRoutes(routeConfig)
  return (
    <div className="app">
      {/* 导航 */}
      <div className="nav">
        <Link to="/">首页</Link>
        <Link to="/news">新闻</Link>
        <Link to="/about">关于我们</Link>
      </div>
      {/* 3. 配置路由 */}
      {element}
    </div>
  )
}
```

`pages/about/index.js`

```diff
+ import { Outlet } from 'react-router-dom'

function About() {
  return (
    <div>
      <h1>About</h1>
      <hr />
      <div className="child">
-        {/* 子路由占位组件 */}
+        <Outlet />
      </div>
    </div>
  )
}

export default About

```

## 动态路由配置

**目标**：能够使用 useRoutes配置动态路由

**内容**：useRoutes配置动态路由

**步骤：**

1. 在根组件App.js中添加子路由规则
2. 在父路由组件使用Outlet组件占位

`App.js`

```diff
import { useRoutes } from 'react-router-dom'
import Home from './pages/home'
import News from './pages/news'
import About from './pages/about'
import Contact from './pages/about/contact'
+ import Detail from './pages/detail'

function App() {
  // 1. 路由配置规则
  const routeConfig = [
    {
      path: '/',
      element: <Home />,
    },
    {
      path: '/news',
      element: <News />,
    },
+    {
+      path: '/detail/:id',
+      element: <Detail />,
+    },
    {
      path: '/about',
      element: <About />,
      children: [{ path: '/about/contact', element: <Contact /> }],
    },
  ]
  // 2. 生成规则配置元素
  const element = useRoutes(routeConfig)
  return (
    <div className="app">
      {/* 导航 */}
      <div className="nav">
        <Link to="/">首页</Link>
        <Link to="/news">新闻</Link>
        <Link to="/about">关于我们</Link>
      </div>
      {/* 3. 配置路由 */}
      {element}
    </div>
  )
}
```



## 404页面配置

**目标**：能够掌握404页面配置

```diff
import { useRoutes } from 'react-router-dom'
import Home from './pages/home'
import News from './pages/news'
import About from './pages/about'
import Contact from './pages/about/contact'
+ import NotFound from './pages/notfound'

function App() {
  // 1. 路由配置规则
  const routeConfig = [
    {
      path: '/',
      element: <Home />,
    },
    {
      path: '/news',
      element: <News />,
    },
    {
      path: '/about',
      element: <About />,
      children: [{ path: '/about/contact', element: <Contact /> }],
    },
+    {
+      path: '*',
+      element: <NotFound />,
+    }
  ]
  // 2. 生成规则配置元素
  const element = useRoutes(routeConfig)
  return (
    <div className="app">
      {/* 导航 */}
      <div className="nav">
        <Link to="/">首页</Link>
        <Link to="/news">新闻</Link>
        <Link to="/about">关于我们</Link>
      </div>
      {/* 3. 配置路由 */}
      {element}
    </div>
  )
}
```

## 路由的执行过程

**目标**：能够说出 react 路由切换页面的执行过程

**内容**：

执行流程：

1. 点击 Link 组件（a标签），修改了浏览器地址栏中的 url
2. React 路由监听到地址栏 url 的变化  hashchange  popstate
3. React 路由内部遍历所有 Route 组件，使用路由规则（path）与 pathname（hash）进行匹配
4. 当路由规则（path）能够匹配地址栏中的 pathname（hash） 时，就展示该 Route 组件的内容

## 编程式导航

**目标**：能够在js中跳转路由

**内容**：

>场景：点击登录按钮，登录成功后，通过代码跳转到后台首页，如何实现？

+  编程式导航：通过 JS 代码来实现页面跳转

> 可以通过 `useNavigate` hook 来拿到路由提供跳转方法

**语法：**`navigate(to:Partial<Location> | string, options:{ replace?: boolean; state?: any })`  

```jsx
import { useNavigate } from 'react-router-dom'

const Login = () => {
  const navigate = useNavigate()
	const onLogin = () => {
    // ...
    // navigate('/') // push
    navigate('/', { replace: true }) // replace
  }
  return (
  	<button onClick={onLogin}>登录</button>
  )
}
```



## 综合案例-黑马头条

接口说明

- 获取频道列表：http://geek.itheima.net/v1_0/channels

- 获取频道新闻：http://geek.itheima.net/v1_0/articles?channel_id=频道id&timestamp=时间戳

### 1. 配置路由

**目标**：创建项目和配置基础路由

**内容**：使用准备好的模板内容搭建项目

**步骤**：

1. 安装 router：`npm i react-router-dom ` 
2. 在根组件配置路由规则

`App.jsx`

```js

import { useRoutes } from 'react-router-dom'
import Home from './pages/home'
import Detail from './pages/detail'

const App = () => {
  const routes = [
    {
      path: '/',
      element: <Home />
    },
    {
      path: '/detail/:id',
      element: <Detail />
    }
  ]
  const element = useRoutes(routes)
  return (
    <div className="app">
      {element}
    </div>
  )
}

export default App
```

`index.js`

```diff
import { createRoot } from 'react-dom/client'
// 样式文件
import './styles/index.css'
import App from './App'

+ import { BrowserRouter } from 'react-router-dom'


createRoot(document.getElementById('root')).render(
+  <BrowserRouter>
    <App />
+  </BrowserRouter>
)
```



### 2. 配置Redux基本结构

**目标**：能够在黑马头条案例中配置Redux

**步骤**：

1. 安装 redux：`npm i @reduxjs/toolkit`
2. 创建store并配置子模块channels.js 

**核心代码**：

store/index.js 中：

```js
import { configureStore } from '@reduxjs/toolkit'
import channels from './modules/channels'

export default configureStore({
  reducer: {
    channels
  }
})
```

store/modules/channels.js .js 中：

```js
import { createSlice } from '@reduxjs/toolkit'

export const channels = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'channels',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [],
    activeId: 0
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
    add (state, action) {
      state.count++
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { add } = news.actions

// 3. 异步action
export const asyncAction = (payload) => {
  return async (dispatch, getState) => {
    dispatch(add())
  }
}

// 导出reducer(创建store使用)
export default channels.reducer
```

### 3. 配置React-Redux

**目标**：能够在案例中配置 react-redux

**步骤**：

1. 安装 react-redux：`npm i react-redux`
2. 在 src/index.js 中，导入 Provider 组件
3. 在 src/index.js 中，导入创建好的 store
4. 使用 Provider 包裹 App 组件，并设置其 store 属性

**核心代码**：

src/index.js 中：

```diff
import { createRoot } from 'react-dom/client'
// 样式文件
import './styles/index.css'
import App from './App'

import { BrowserRouter } from 'react-router-dom'
+ import { Provider } from 'react-redux'
+ import store from './store'

createRoot(document.getElementById('root')).render(
+  <Provider store={store}>
    <BrowserRouter>
      <App />
    </BrowserRouter>
+  </Provider>
)

```

### 5. 获取频道数据

**目标**：能够获取频道数据

**分析说明**：

对于频道数据来说，需要进入页面时就获取。因此，可以通过 `useEffect` hook 来实现。

**步骤：**

1. 定义异步action函数获取频道数据并存储
2. 组件中分发异步action获取数据

**核心代码**：

Channel.js 中：

```jsx
import { useEffect } from 'react'
import { useDispatch } from 'react-redux'
import { getChannelAction } from '../../../store/modules/channels'

export const Channel = () => {
  const dispatch = useDispatch()

  useEffect(() => {
    dispatch(getChannelAction())
  }, [dispatch])	
  // ...
}
```

actions/channels.js 中：

```diff
import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

export const channels = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'channels',
  // 1. 定义变量(状态数据)
  initialState: {
    list: [],
    activeId: 0
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
+    setList (state, action) {
+      state.list = action.payload
+    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { setList } = channels.actions

- // 3. 获取频道数据
+ export const getChannelAction = (payload) => {
+  return async (dispatch, getState) => {
+    const { data } = await axios.get('http://toutiao.itheima.net/v1_0/channels')
+    dispatch(setList(data.data.channels))
+  }
+}

// 导出reducer(创建store使用)
export default channels.reducer
```

### 6. 渲染频道

**目标**：能够渲染频道数据

**步骤**：

1. 在 Channel 组件中获取频道数据，并渲染

**核心代码**：

Channel.js 中：

```jsx
import { useSelector } from 'react-redux'

export const Channel = () => {
  const dispatch = useDispatch()
  const { list } = useSelector(state => state.channels)

  return (
    <ul className="category">
      {list.map(item => (
        <li
          key={item.id}
          className="select"
        >
          {item.name}
        </li>
      ))}
    </ul>
  )
}
```

### 7. 点击频道高亮

**目标**：能够实现点击频道高亮

**步骤**：

1. 在 Channel 组件中拿到高亮频道的 id，为频道项添加高亮类名
2. 为频道项绑定点击事件，在点击事件中，分发切换高亮的 action
4. 创建切换高亮的 action

**核心代码**：

Channel.js 中：

```jsx
import { useSelector } from 'react-redux'
import { getChannelAction, setActiveAction } from '../../../store/modules/channels'


export const Channel = () => {
  const { list, activeId } = useSelector(state => state.channels)

  return (
		// ...
    <li
      className={activeId === item.id ? "select" : ''}
      onClick={() => dispatch(setActiveAction(item.id))}
    >
      {item.name}
    </li>
  )
}
```

modules/channels.js 中：

```js
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
    // 设置高亮ID
    setActiveAction (state, action) {
      state.activeId = action.payload
    }
 }

export const { setList, setActiveAction } = channels.actions
```

### 8. 获取文章列表数据（作业）

**目标**：能够获取文章列表数据

**步骤**：

1. 在 ArticleList 组件中通过 useEffect hook 分发获取文章数据的 action
3. 创建子模块news.js
3. 定义异步action获取新闻数据

**核心代码**：

ArticleList.js 中：

```jsx
import { useEffect } from 'react'
import { useDispatch, useSelector } from 'react-redux'
import { getNewsAction } from '../../../store/modules/news'

export const ArticleList = () => {
  const dispatch = useDispatch()
  // 获取频道ID，根据频道ID获取文章列表
  const { activeId } = useSelector((state) => state.channels)

  useEffect(() => {
    dispatch(getNewsAction(activeId))
  }, [dispatch, activeId])
}
```

modules/news.js 中：

```js
import { createSlice } from '@reduxjs/toolkit'
import axios from 'axios'

export const news = createSlice({
  // 命名空间，在调用action的时候会默认的设置为action命令的前缀,避免冲突
  name: 'news',
  // 1. 定义变量(状态数据)
  initialState: {
    list: []
  },
  // 2. 定义reducers更新变量(其中函数属性名作为action，在组件中可以dispatch触发reducer函数更新状态)
  reducers: {
    // action函数(同步)
    setListAction (state, action) {
      state.list = action.payload
    }
  },
})

// 导出action函数(组件中可以dispatch(action函数)触发reducer更新状态)
export const { setListAction } = news.actions

// 3. 异步action
export const getNewsAction = (id) => {
  return async (dispatch, getState) => {
    const { data } = await axios.get(
      `http://toutiao.itheima.net/v1_0/articles?channel_id=${id}&timestamp=${Date.now()}`
    )
    dispatch(setListAction(data.data.results))
  }
}

// 导出reducer(创建store使用)
export default news.reducer
```

`store/index.js`

```diff
import { configureStore } from '@reduxjs/toolkit'
import channels from './modules/channels'
+ import news from './modules/news'

export default configureStore({
  reducer: {
    channels,
+   news
  }
})
```



### 9. 渲染文章列表（作业）

**目标**：能够渲染文章列表数据

**步骤**：

1. 在 ArticleList 组件中获取频道数据，并渲染

**核心代码**：

ArticleList.js 中：

```jsx
import { useSelector } from 'react-redux'

export const ArticleList = () => {
  // 文章列表数据
  const { list } = useSelector(state => state.news)

  return (
    <div className="list">
      {list.map(item => (
        <div key={item.art_id} className="article_item">
          <h3>{item.title}</h3>
          <div className="img_box">
            <img
              src={item.cover.type === 0 ? avatar : item.cover.images[0]}
              className="w100"
              alt=""
            />
          </div>
          <div className="info_box">
            <span>{item.aut_name}</span>
            <span>{item.comm_count}评论</span>
            <span>{item.pubdate}</span>
          </div>
        </div>
      ))}
    </div>
  )
}
```

## 第七天



